#!/bin/bash

TEST_FILE=$1

R --slave <<-EOF
	library('RUnit')
	library('pd.huex.1.0.st.v2')
	res <- runTestFile('${TEST_FILE}',
        	rngKind='default', rngNormalKind='default')
	printTextProtocol(res, showDetails=FALSE)
EOF
