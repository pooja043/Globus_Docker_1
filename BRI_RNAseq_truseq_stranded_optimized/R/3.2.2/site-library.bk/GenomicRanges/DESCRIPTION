Package: GenomicRanges
Title: Representation and manipulation of genomic intervals
Description: The ability to efficiently represent and manipulate genomic
	annotations and alignments is playing a central role when it comes
	to analyze high-throughput sequencing data (a.k.a. NGS data).
	The package defines general purpose containers for storing genomic
	intervals. Specialized containers for representing and manipulating
	short alignments against a reference genome are defined in the
	GenomicAlignments package.
Version: 1.16.4
Author: P. Aboyoun, H. Pages and M. Lawrence
Maintainer: Bioconductor Package Maintainer <maintainer@bioconductor.org>
biocViews: Genetics, Infrastructure, Sequencing, Annotation
Depends: R (>= 2.10), methods, BiocGenerics (>= 0.7.7), IRanges (>=
        1.21.33), GenomeInfoDb (>= 0.99.17)
Imports: methods, utils, stats, BiocGenerics, IRanges, XVector
LinkingTo: IRanges, XVector (>= 0.3.4)
Suggests: AnnotationDbi (>= 1.21.1), AnnotationHub, BSgenome,
        BSgenome.Hsapiens.UCSC.hg19, BSgenome.Scerevisiae.UCSC.sacCer2,
        Biostrings (>= 2.25.3), Rsamtools (>= 1.13.53),
        GenomicAlignments, rtracklayer, KEGG.db, KEGGgraph,
        GenomicFeatures, TxDb.Dmelanogaster.UCSC.dm3.ensGene,
        TxDb.Hsapiens.UCSC.hg19.knownGene,
        TxDb.Athaliana.BioMart.plantsmart21, seqnames.db,
        org.Sc.sgd.db, VariantAnnotation, edgeR, DESeq, DEXSeq,
        pasilla, pasillaBamSubset, RUnit, digest, BiocStyle
License: Artistic-2.0
Collate: utils.R phicoef.R transcript-utils.R constraint.R
        makeSeqnameIds.R seqinfo.R strand-utils.R range-squeezers.R
        Seqinfo-class.R GenomicRanges-class.R GRanges-class.R
        GIntervalTree-class.R GenomicRanges-comparison.R
        GenomicRangesList-class.R GRangesList-class.R
        makeGRangesFromDataFrame.R tileGenome.R
        SummarizedExperiment-class.R
        SummarizedExperiment-rowData-methods.R seqlevels-utils.R
        resolveHits-methods.R RangesMapping-methods.R
        RangedData-methods.R intra-range-methods.R
        inter-range-methods.R coverage-methods.R setops-methods.R
        findOverlaps-methods.R findOverlaps-GIntervalTree-methods.R
        nearest-methods.R map-methods.R tile-methods.R
        test_GenomicRanges_package.R zzz.R
Packaged: 2014-08-02 02:28:26 UTC; biocbuild
Built: R 3.2.2; x86_64-pc-linux-gnu; 2015-08-19 18:11:16 UTC; unix
