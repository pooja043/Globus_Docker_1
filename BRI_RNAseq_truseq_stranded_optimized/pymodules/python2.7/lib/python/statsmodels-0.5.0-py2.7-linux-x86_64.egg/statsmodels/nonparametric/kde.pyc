ó
_âTc           @@  sÆ  d  Z  d d l m Z d d l Z d d l Z d d l m Z m Z d d l	 m
 Z
 d d l m Z m Z d d l m Z d d	 l m Z m Z m Z m Z d d
 l m Z e d e
 j d e
 j d e
 j d e
 j d e
 j d e
 j d e
 j  Z d   Z  d e! f d     YZ" d e" f d     YZ# d d d d d e j% e j% f d e& d  Z' d d d d d e j% e j% f d e& d  Z( e) d k rÂd d l Z e j* j+ d  e j* j, d  Z- e' e- d d d d  d! e& \ Z. Z/ Z0 e( e- d d d d" d! e1 \ Z2 Z3 y¦ d d# l4 m5 Z5 d$ Z6 d% Z7 e7 e6 Z8 e j9 e-  Z: e5 e- e6 e7 e: e j; d&  e j; d&  d e j; d&  e j; d&  	 \ Z< Z= Z> Z? Z@ e jA jB e2 e= d  WqÂd' GHqÂXn  d S((   s`  
Univariate Kernel Density Estimators

References
----------
Racine, Jeff. (2008) "Nonparametric Econometrics: A Primer," Foundation and
    Trends in Econometrics: Vol 3: No 1, pp1-88.
    http://dx.doi.org/10.1561/0800000009

http://en.wikipedia.org/wiki/Kernel_%28statistics%29

Silverman, B.W.  Density Estimation for Statistics and Data Analysis.
i    (   t   absolute_importN(   t	   integratet   stats(   t   kernels(   t   cache_readonlyt   resettable_cachei   (   t
   bandwidths(   t   forrtt   revrtt   silverman_transformt   counts(   t   fast_linbint   gaut   epat   unit   trit   biwt   triwt   cosc         C@  s%   y |  j  Wn t d   n Xd  S(   Ns!   Call fit to fit the density first(   t   densityt
   ValueError(   t   self(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt   _checkisfit"   s    t   KDEUnivariatec        	   B@  s   e  Z d  Z d   Z d d e d d d d e j e j f d  Z e	 d    Z
 e	 d    Z e	 d	    Z e	 d
    Z e	 d    Z d   Z RS(   sÒ  
    Univariate Kernel Density Estimator.

    Parameters
    ----------
    endog : array-like
        The variable for which the density estimate is desired.

    Notes
    -----
    If cdf, sf, cumhazard, or entropy are computed, they are computed based on
    the definition of the kernel rather than the FFT approximation, even if
    the density is fit with FFT = True.

    `KDEUnivariate` is much faster than `KDEMultivariate`, due to its FFT-based
    implementation.  It should be preferred for univariate, continuous data.
    `KDEMultivariate` also supports mixed data.

    See Also
    --------
    KDEMultivariate
    kdensity, kdensityfft

    Examples
    --------
    >>> import statsmodels.api as sm
    >>> import matplotlib.pyplot as plt

    >>> nobs = 300
    >>> np.random.seed(1234)  # Seed random generator
    >>> dens = sm.nonparametric.KDEUnivariate(np.random.normal(size=nobs))
    >>> dens.fit()
    >>> plt.plot(dens.cdf)
    >>> plt.show()

    c         C@  s   t  j |  |  _ d  S(   N(   t   npt   asarrayt   endog(   R   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt   __init__R   s    R   t   scotti   i   c	         C@  s?  y t  |  } d |  _ Wn | |  _ n X|  j }	 | r¿ | d k r\ d }
 t |
   n  | d k	 r} d }
 t |
   n  t |	 d | d | d | d | d	 | d
 | d | \ } } } n? t |	 d | d | d | d | d	 | d
 | d | \ } } } | |  _ | |  _ | |  _	 t
 | d |  |  _ t   |  _ d S(   s  
        Attach the density estimate to the KDEUnivariate class.

        Parameters
        ----------
        kernel : str
            The Kernel to be used. Choices are:

            - "biw" for biweight
            - "cos" for cosine
            - "epa" for Epanechnikov
            - "gau" for Gaussian.
            - "tri" for triangular
            - "triw" for triweight
            - "uni" for uniform

        bw : str, float
            The bandwidth to use. Choices are:

            - "scott" - 1.059 * A * nobs ** (-1/5.), where A is
              `min(std(X),IQR/1.34)`
            - "silverman" - .9 * A * nobs ** (-1/5.), where A is
              `min(std(X),IQR/1.34)`
            - If a float is given, it is the bandwidth.

        fft : bool
            Whether or not to use FFT. FFT implementation is more
            computationally efficient. However, only the Gaussian kernel
            is implemented. If FFT is False, then a 'nobs' x 'gridsize'
            intermediate array is created.
        gridsize : int
            If gridsize is None, max(len(X), 50) is used.
        cut : float
            Defines the length of the grid past the lowest and highest values
            of X so that the kernel goes to zero. The end points are
            -/+ cut*bw*{min(X) or max(X)}
        adjust : float
            An adjustment factor for the bw. Bandwidth becomes bw * adjust.
        s
   user-givenR   s)   Only gaussian kernel is available for ffts#   Weights are not implemented for fftt   kernelt   bwt   adjustt   weightst   gridsizet   clipt   cutt   hN(   t   floatt	   bw_methodR   t   NotImplementedErrort   Nonet   kdensityfftt   kdensityR   t   supportR   t   kernel_switchR   R   t   _cache(   R   R   R   t   fftR    R!   R   R#   R"   R   t   msgR   t   grid(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt   fitU   s0    )				c   
      @  så   t  |   |  j } |  j     j d k rB t j t j } } n   j \ } }   f d   } |  j } t j | | f } t	 |  } |  j
 } g  t d |  D]1 } t j | | | d | | d | d ^ q¡ }	 t j |	  S(   s§   
        Returns the cumulative distribution function evaluated at the support.

        Notes
        -----
        Will not work if fit has not been called.
        c         @  s     j  | |   S(   N(   R   (   t   xt   s(   t   kern(    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt   <lambda>«   s    i   t   argsi    N(   R   R   R   t   domainR(   R   t   infR+   t   r_t   lenR   t   xrangeR   t   quadt   cumsum(
   R   R   t   at   bt   funcR+   R!   R   t   it   probs(    (   R4   s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt   cdf   s    	
				Dc         C@  s   t  |   t j |  j  S(   s   
        Returns the hazard function evaluated at the support.

        Notes
        -----
        Will not work if fit has not been called.

        (   R   R   t   logt   sf(   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt	   cumhazardµ   s    

c         C@  s   t  |   d |  j S(   s   
        Returns the survival function evaluated at the support.

        Notes
        -----
        Will not work if fit has not been called.
        i   (   R   RC   (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyRE   Â   s    	
c         @  s   t  |     f d   } |  j } |  j     j d k	 rL |  j \ } } n t j t j } } |  j } t j	 | | | d | f d S(   sê   
        Returns the differential entropy evaluated at the support

        Notes
        -----
        Will not work if fit has not been called. 1e-12 is added to each
        probability to ensure that log(0) is not called.
        c         @  s'     j  | |   } | t j | d  S(   Ngê-q=(   R   R   RD   (   R2   R3   t   pdf(   R4   (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt   entrÚ   s    R6   i    N(
   R   R   R   R7   R(   R   R8   R   R   R<   (   R   RH   RG   R>   R?   R   (    (   R4   s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt   entropyÎ   s    

			c         C@  s>   t  |   t |  j  } t j j |  j t j d d |   S(   sÂ   
        Inverse Cumulative Distribution (Quantile) Function

        Notes
        -----
        Will not work if fit has not been called. Uses
        `scipy.stats.mstats.mquantiles`.
        i    i   (	   R   R:   R   R   t   mstatst
   mquantilesR   R   t   linspace(   R   R!   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt   icdfé   s    

c         C@  s    t  |   |  j j |  j |  S(   s¦   
        Evaluate density at a single point.

        Parameters
        ----------
        point : float
            Point at which to evaluate the density.
        (   R   R   R   R   (   R   t   point(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt   evaluateø   s    	
N(   t   __name__t
   __module__t   __doc__R   t   TrueR(   R   R8   R1   R   RC   RF   RE   RI   RM   RO   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyR   ,   s   $	"Et   KDEc           B@  s   e  Z d    Z RS(   c         C@  s&   t  j |  |  _ t j d t  d  S(   NsG   KDE is deprecated and will be removed in 0.6, use KDEUnivariate instead(   R   R   R   t   warningst   warnt   FutureWarning(   R   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyR     s    	(   RP   RQ   R   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyRT     s   R   i   c	         C@  sd  t  j |   }  |  j d k r7 |  d d  d f }  n  t  j |  | d k |  | d k   }	 |  |	 }  t t |    }
 | d k r t |
 d  } n  | d k r» t  j |
  } |
 } nI t |  t |	  k rè d } t	 |   n  | |	 j
   } | j   } y t |  } Wn t j |  | |  } n X| | 9} t  j |  d d | | } t  j |  d d | | } t  j | | |  } |  j | d d  d f | } t | d |  } | j d k	 r| j \ } } | | k  | | k B} | |  } d | | <n | |  } d | | d k  <t  j | |  | | } | rV| | | f S| | f Sd S(   sÅ  
    Rosenblatt-Parzen univariate kernel density estimator.

    Parameters
    ----------
    X : array-like
        The variable for which the density estimate is desired.
    kernel : str
        The Kernel to be used. Choices are
        - "biw" for biweight
        - "cos" for cosine
        - "epa" for Epanechnikov
        - "gau" for Gaussian.
        - "tri" for triangular
        - "triw" for triweight
        - "uni" for uniform
    bw : str, float
        "scott" - 1.059 * A * nobs ** (-1/5.), where A is min(std(X),IQR/1.34)
        "silverman" - .9 * A * nobs ** (-1/5.), where A is min(std(X),IQR/1.34)
        If a float is given, it is the bandwidth.
    weights : array or None
        Optional  weights. If the X value is clipped, then this weight is
        also dropped.
    gridsize : int
        If gridsize is None, max(len(X), 50) is used.
    adjust : float
        An adjustment factor for the bw. Bandwidth becomes bw * adjust.
    clip : tuple
        Observations in X that are outside of the range given by clip are
        dropped. The number of observations in X is then shortened.
    cut : float
        Defines the length of the grid past the lowest and highest values of X
        so that the kernel goes to zero. The end points are
        -/+ cut*bw*{min(X) or max(X)}
    retgrid : bool
        Whether or not to return the grid over which the density is estimated.

    Returns
    -------
    density : array
        The densities estimated at the grid points.
    grid : array, optional
        The grid points at which the density is estimated.

    Notes
    -----
    Creates an intermediate (`gridsize` x `nobs`) array. Use FFT for a more
    computationally efficient version.
    i   Ni    i2   s:   The length of the weights must be the same as the given X.t   axisR$   (   R   R   t   ndimR(   t   logical_andR%   R:   t   maxt   onesR   t   squeezet   sumR   t   select_bandwidtht   minRL   t   TR,   R7   t   dot(   t   XR   R   R    R!   R   R"   R#   t   retgridt   clip_xt   nobst   qR/   R>   R?   R0   t   kR4   t   z_lot   z_hight   domain_maskt   dens(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyR*     sJ    3&
	
!c	         C@  s  t  j |   }  |  t  j |  | d k |  | d k   }  y t |  } Wn t j |  | |  } n X| | 9} t t |    }	 | d k r¨ t  j |	 d f  } n  d t  j	 t  j
 |   } t  j |   | | }
 t  j |   | | } t  j |
 | | d t \ } } | |
 } t |  |
 | |  | |	 } t |  } t | | |  | } t |  } | r{| | | f S| | f Sd S(   sú
  
    Rosenblatt-Parzen univariate kernel density estimator

    Parameters
    ----------
    X : array-like
        The variable for which the density estimate is desired.
    kernel : str
        ONLY GAUSSIAN IS CURRENTLY IMPLEMENTED.
        "bi" for biweight
        "cos" for cosine
        "epa" for Epanechnikov, default
        "epa2" for alternative Epanechnikov
        "gau" for Gaussian.
        "par" for Parzen
        "rect" for rectangular
        "tri" for triangular
    bw : str, float
        "scott" - 1.059 * A * nobs ** (-1/5.), where A is min(std(X),IQR/1.34)
        "silverman" - .9 * A * nobs ** (-1/5.), where A is min(std(X),IQR/1.34)
        If a float is given, it is the bandwidth.
    weights : array or None
        WEIGHTS ARE NOT CURRENTLY IMPLEMENTED.
        Optional  weights. If the X value is clipped, then this weight is
        also dropped.
    gridsize : int
        If gridsize is None, min(len(X), 512) is used. Note that the provided
        number is rounded up to the next highest power of 2.
    adjust : float
        An adjustment factor for the bw. Bandwidth becomes bw * adjust.
        clip : tuple
        Observations in X that are outside of the range given by clip are
        dropped. The number of observations in X is then shortened.
    cut : float
        Defines the length of the grid past the lowest and highest values of X
        so that the kernel goes to zero. The end points are
        -/+ cut*bw*{X.min() or X.max()}
    retgrid : bool
        Whether or not to return the grid over which the density is estimated.

    Returns
    -------
    density : array
        The densities estimated at the grid points.
    grid : array, optional
        The grid points at which the density is estimated.

    Notes
    -----
    Only the default kernel is implemented. Weights aren't implemented yet.
    This follows Silverman (1982) with changes suggested by Jones and Lotwick
    (1984). However, the discretization step is replaced by linear binning
    of Fan and Marron (1994). This should be extended to accept the parts
    that are dependent only on the data to speed things up for
    cross-validation.

    References
    ---------- ::

    Fan, J. and J.S. Marron. (1994) `Fast implementations of nonparametric
        curve estimators`. Journal of Computational and Graphical Statistics.
        3.1, 35-56.
    Jones, M.C. and H.W. Lotwick. (1984) `Remark AS R50: A Remark on Algorithm
        AS 176. Kernal Density Estimation Using the Fast Fourier Transform`.
        Journal of the Royal Statistical Society. Series C. 33.1, 120-2.
    Silverman, B.W. (1982) `Algorithm AS 176. Kernel density estimation using
        the Fast Fourier Transform. Journal of the Royal Statistical Society.
        Series C. 31.2, 93-9.
    i    i   g      @i   t   retstepN(   R   R   RZ   R%   R   R_   R:   R(   R[   t   ceilt   log2R`   RL   RS   R   R   R	   R   (   Rc   R   R   R    R!   R   R"   R#   Rd   Rf   R>   R?   R0   t   deltat   RANGEt   binnedt   yt   zstart   f(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyR)   x  s,    G*
!
t   __main__i90  id   R   R   gÅÄæãÚ×?Rd   t	   silverman(   t   denestg\çIRèÀgñ½ÞJöw@i   s5   Didn't get the estimates from the Silverman algorithm(C   RR   t
   __future__R    RU   t   numpyR   t   scipyR   R   t!   statsmodels.sandbox.nonparametricR   t   statsmodels.tools.decoratorsR   R   t    R   t   kdetoolsR   R   R	   R
   t   linbinR   t   dictt   Gaussiant   Epanechnikovt   Uniformt
   Triangulart   Biweightt	   Triweightt   CosineR,   R   t   objectR   RT   R(   R8   RS   R*   R)   RP   t   randomt   seedt   randnt   xiRu   R0   t   bw1t   Falset   f2t   bw2t   denest2Rx   R>   R?   Rq   t   bw_silvermanR   t   zerost   ftt   smootht   ifaultR    t   smooth1t   testingt   assert_almost_equal(    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/nonparametric/kde.pyt   <module>   sJ   "	
Ù	"i"}'$
*-