ó
ÛâTc           @   s²   d  Z  d Z d Z d d l j j Z d d l j j	 Z
 d d l j Z d d l j Z d d l j Z d d l Z d d l Td d l m Z m Z m Z d e j f d     YZ d S(	   s*   Andre Merzky, Ole Weidner, Alexander Grills%   Copyright 2012-2013, The SAGA Projectt   MITiÿÿÿÿN(   t   *(   t   SYNCt   ASYNCt   TASKt	   Directoryc           B   s«  e  Z d  Z e j d e j e j e f  e j e	 e j
  e j e j  e j e j  e j e  e j e j e e e    e j e j
  d d d d i  d d    Z e e j d e j e j e f  e j e	 e j
  e j e j  e j e j e e e    e j e j  d d d d d     Z e j d e j e f e j e	 e j
  e j e j e e e    e j e j e j f  d d d    Z e j d e j e f e j e	 e j
  e j e j e e e    e j d e j f  d d d    Z e j d e j e f e j e	 e j
  e j e j e e e    e j e j
 e j f  d d d    Z e j d e j e f e j e	 e j
  e j e j e e e    e j e j
 e j f  d d d    Z  e j d e j e  e j e	 e j
  e j e j e e e    e j e j! e j  e j f  d d d d	    Z" e j d e j e f e j e j e e e    e j e# e j f  d d
    Z$ e j d e j e  e j e	 e j
  e j e j e e e    e j e j! e j  e j f  e% d d    Z& e j d e j e j e e e    e j e	 e j f  d d    Z' e j d e	 e j e j e e e    e j e j e j f  d d    Z( e j d e j e f e j e j e f  e j e	 e j
  e j e j e e e    e j e j
 e j f  d d d d    Z) e j d e j e f e j e j e f  e j e	 e j
  e j e j e e e    e j e j
 e j f  d d d d    Z* e j d e j e f e j e j e f  e j e	 e j
  e j e j e e e    e j e j
 e j f  d d d d    Z+ e j d e j e f e j e	 e j
  e j e j e e e    e j e j
 e j f  d d d d    Z, e j d e j e j e f  e j e j e e e    e j e# e j f  d d d    Z- e j d e j e j e f  e j e j e e e    e j e# e j f  d d d    Z. e j d e j e j e f  e j e j e e e    e j e# e j f  d d d    Z/ e j d e j e j e f  e j e j e e e    e j e j e j f  d d d    Z0 RS(   sA  
    Represents a SAGA directory as defined in GFD.90
    
    The saga.namespace.Directory class represents, as the name indicates,
    a directory on some (local or remote) namespace.  That class offers
    a number of operations on that directory, such as listing its contents,
    copying entries, or creating subdirectories::
    
        # get a directory handle
        dir = saga.namespace.Directory("sftp://localhost/tmp/")
    
        # create a subdir
        dir.make_dir ("data/")
    
        # list contents of the directory
        entries = dir.list ()
    
        # copy *.dat entries into the subdir
        for f in entries :
            if f ^ '^.*\.dat$' :
                dir.copy (f, "sftp://localhost/tmp/data/")


    Implementation note:
    ^^^^^^^^^^^^^^^^^^^^

    The SAGA API Specification (GFD.90) prescribes method overloading on method
    signatures, but that is not supported by Python (Python only does method
    overwriting).  So we implement one generic method version here, and do the
    case switching based on the provided parameter set.
    R   c         C   sG   | s d } n  t  t |   |  _ |  j j | | | | | d | d S(   s£  
        :param url: Url of the (remote) entry system directory.
        :type  url: :class:`saga.Url` 

        flags:     flags enum
        session:   saga.Session
        ret:       obj
        
        Construct a new directory object

        The specified directory is expected to exist -- otherwise
        a DoesNotExist exception is raised.  Also, the URL must point to
        a directory (not to an entry), otherwise a BadParameter exception is
        raised.

        Example::

            # open some directory
            dir = saga.namespace.Directory("sftp://localhost/tmp/")

            # and list its contents
            entries = dir.list ()

        i    t   _ttypeN(   t   superR   t   _nsentryt   __init__(   t   selft   urlt   flagst   sessiont   _adaptort   _adaptor_stateR   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR	   9   s
    # 	c         C   s7   | s d } n  t  t |   } | j | | | d | S(   s¶   
        url:       saga.Url
        flags:     saga.namespace.flags enum
        session:   saga.Session
        ttype:     saga.task.type enum
        ret:       saga.Task
        i    t   ttype(   R   R   t   create(   t   clsR   R   R   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR   d   s     	c         C   s7   | s d } n  t  j |  } |  j j | | d | S(   s©   
        name:     saga.Url
        flags:    saga.namespace.flags enum
        ttype:    saga.task.type enum
        ret:      saga.namespace.Entry / saga.Task
        i    R   (   t   surlt   UrlR   t   open(   R
   t   nameR   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR   {   s     	c         C   s1   | s d } n  |  j  j t j |  | d | S(   s  
        :param path: name/path of the directory to open
        :param flags: directory creation flags

        ttype:    saga.task.type enum
        ret:      saga.namespace.Directory / saga.Task
        
        Open and return a new directoy

           The call opens and returns a directory at the given location.

           Example::

               # create a subdir 'data' in /tmp
               dir = saga.namespace.Directory("sftp://localhost/tmp/")
               data = dir.open_dir ('data/', saga.namespace.Create)
        i    R   (   R   t   open_dirR   R   (   R
   t   pathR   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR      s     	i    c         C   s1   | s d } n  |  j  j t j |  | d | S(   sÍ  
        :param tgt:   name/path of the new directory
        :param flags: directory creation flags

        ttype:         saga.task.type enum
        ret:           None / saga.Task
        
        Create a new directoy

        The call creates a directory at the given location.

        Example::

            # create a subdir 'data' in /tmp
            dir = saga.namespace.Directory("sftp://localhost/tmp/")
            dir.make_dir ('data/')
        i    R   (   R   t   make_dirR   R   (   R
   t   tgtR   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR   ­   s     	c         C   s+   | s d } n  |  j  j | d | d | S(   s   
        url:           saga.Url
        flags:         flags enum
        ttype:         saga.task.type enum
        ret:           None / saga.Task
        i    R   R   (   R   t
   change_dir(   R
   R   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR   Ê   s     	c         C   s(   | s d } n  |  j  j | | d | S(   sÎ  
        :param pattern: Entry name pattern (like POSIX 'ls', e.g. '\*.txt')

        flags:         flags enum
        ttype:         saga.task.type enum
        ret:           list [saga.Url] / saga.Task
        
        List the directory's content

        The call will return a list of entries and subdirectories within the
        directory::

            # list contents of the directory
            for f in dir.list() :
                print f
        i    R   (   R   t   list(   R
   t   patternR   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR   Ü   s     	c         C   s   |  j  j | d | S(   s  
        :param path: path of the entry to check

        ttype:         saga.task.type enum
        ret:           bool / saga.Task
        
        Returns True if path exists, False otherwise. 


        Example::

            # inspect an entry
            dir  = saga.namespace.Directory("sftp://localhost/tmp/")
            if dir.exists ('data'):
                # do something
        R   (   R   t   exists(   R
   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR   ø   s    c         C   s(   | s d } n  |  j  j | | d | S(   s§   
        pattern:       string
        flags:         flags enum
        ttype:         saga.task.type enum
        ret:           list [saga.Url] / saga.Task
        i    R   (   R   t   find(   R
   R   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR     s     	c         C   s   |  j  j d |  S(   s[   
        ttype:         saga.task.type enum
        ret:           int / saga.Task
        R   (   R   t   get_num_entries(   R
   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR    %  s    c         C   s   |  j  j | d | S(   s|   
        num:           int 
        ttype:         saga.task.type enum
        ret:           saga.Url / saga.Task
        R   (   R   t	   get_entry(   R
   t   numR   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR!   2  s    
c         C   sN   | s d } n  | r1 |  j  j | | | d | S|  j j | | d | Sd S(   s  
        :param src: path of the entry to copy
        :param tgt: absolute URL of target name or directory
        
        url_1:         saga.Url
        url_2:         saga.Url / None
        flags:         flags enum / None
        ttype:         saga.task.type enum / None
        ret:           None / saga.Task
        
        Copy an entry from source to target

        The source is copied to the given target directory.  The path of the
        source can be relative::

            # copy an entry
            dir = saga.namespace.Directory("sftp://localhost/tmp/")
            dir.copy ("./data.bin", "sftp://localhost/tmp/data/")
        i    R   N(   R   t   copyR   (   R
   t   url_1t   url_2R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR#   C  s
     	 c         C   sN   | s d } n  | r1 |  j  j | | | d | S|  j j | | d | Sd S(   s¾   
        src:           saga.Url
        tgt:           saga.Url
        flags:         flags enum
        ttype:         saga.task.type enum
        ret:           None / saga.Task
        i    R   N(   R   t   linkR   (   R
   R$   R%   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR&   g  s
     	 c         C   sN   | s d } n  | r1 |  j  j | | | d | S|  j j | | d | Sd S(   s)  
        :param src: path of the entry to copy
        :param tgt: absolute URL of target directory

        flags:         flags enum
        ttype:         saga.task.type enum
        ret:           None / saga.Task
        
        Move an entry from source to target

        The source is moved to the given target directory.  The path of the
        source can be relative::

            # copy an entry
            dir = saga.namespace.Directory("sftp://localhost/tmp/")
            dir.move ("./data.bin", "sftp://localhost/tmp/data/")

        i    R   N(   R   t   moveR   (   R
   R$   R%   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR'   |  s
     	 c         C   sH   | s d } n  | r. |  j  j | | d | S|  j j | d | Sd S(   s   
        tgt:           saga.Url
        flags:         flags enum
        ttype:         saga.task.type enum
        ret:           None / saga.Task
        i    R   N(   R   t   removeR   (   R
   R   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR(     s
     	 c         C   s3   | r |  j  j | d | S|  j j d |  Sd S(   s  
        tgt:           saga.Url / None
        ttype:         saga.task.type enum
        ret:           bool / saga.Task
        
        Returns True if path is a directory, False otherwise. 

        Example::

            # inspect an entry
            dir  = saga.namespace.Directory("sftp://localhost/tmp/")
            if dir.is_dir ('data'):
                # do something
        R   N(   R   t   is_dirR   (   R
   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR)   ¯  s     c         C   s3   | r |  j  j | d | S|  j j d |  Sd S(   s   
        tgt:           saga.Url / None
        ttype:         saga.task.type enum
        ret:           bool / saga.Task
        R   N(   R   t   is_entryR   (   R
   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR*   È  s    
 c         C   s3   | r |  j  j | d | S|  j j d |  Sd S(   s   
        tgt:           saga.Url / None
        ttype:         saga.task.type enum
        ret:           bool / saga.Task
        R   N(   R   t   is_linkR   (   R
   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR+   Ø  s    
 c         C   s3   | r |  j  j | d | S|  j j d |  Sd S(   s   
        tgt:           saga.Url / None
        ttype:         saga.task.type enum
        ret:           saga.Url / saga.Task
        R   N(   R   t	   read_linkR   (   R
   R   R   (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR,   è  s     N(1   t   __name__t
   __module__t   __doc__t   rust   takest   optionalR   R   t
   basestringt   intt   nothingt   sst   Sessiont   sabt   Baset   dictt   one_ofR   R   R   t   returnst   NoneR	   t   classmethodt   stt   TaskR   t   entryt   EntryR   R   R   R   t   list_ofR   t   boolR   t	   RECURSIVER   R    R!   R#   R&   R'   R(   R)   R*   R+   R,   (    (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyR      sè   		"						!		!	
									(   t
   __author__t   __copyright__t   __license__t   radical.utils.signaturest   utilst
   signaturesR0   t   saga.adaptors.baset   adaptorst   baseR8   t   saga.sessionR   R6   t	   saga.taskt   taskR?   t   saga.urlR   R   RA   t   saga.namespace.constantst   saga.constantsR   R   R   RB   R   (    (    (    sl   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/namespace/directory.pyt   <module>   s   
