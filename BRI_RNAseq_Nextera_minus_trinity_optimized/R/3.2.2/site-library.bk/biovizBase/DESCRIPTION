Package: biovizBase
Version: 1.10.8
Title: Basic graphic utilities for visualization of genomic data.
Description: The biovizBase package is designed to provide a set of utilities, color schemes and conventions for genomic data. It serves as the base for various high-level packages for biological data visualization. This saves development effort and encourages consistency.
Author: Tengfei Yin, Michael Lawrence, Dianne Cook
Maintainer: Tengfei Yin <yintengfei@gmail.com>
Depends: R (>= 2.10), methods
Imports: methods, grDevices, stats, scales, Hmisc, RColorBrewer,
        dichromat, BiocGenerics, IRanges, GenomicRanges (>= 1.13.3),
        Biostrings, Rsamtools (>= 1.13.1), GenomicFeatures
Suggests: BSgenome.Hsapiens.UCSC.hg19,
        TxDb.Hsapiens.UCSC.hg19.knownGene, BSgenome, rtracklayer
biocViews: Infrastructure, Visualization, Bioinformatics, Preprocessing
License: Artistic-2.0
LazyLoad: Yes
Collate: utils.R color.R AllGenerics.R crunch-method.R
        addStepping-method.R getFragLength-method.R
        shrinkageFun-method.R maxGap-method.R spliceSummary-method.R
        ideogram.R pileup.R coverage.R labs.R original.R transform.R
        facets-method.R aes.R scale.R zzz.R biovizBase-package.R
Packaged: 2014-03-01 05:37:28 UTC; biocbuild
Built: R 3.0.0; x86_64-unknown-linux-gnu; 2015-07-27 17:34:47 UTC; unix
