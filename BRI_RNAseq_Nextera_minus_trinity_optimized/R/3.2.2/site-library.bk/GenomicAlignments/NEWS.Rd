\name{GenomicAlignments-NEWS}
\title{GenomicAlignments News}

\section{CHANGES IN VERSION 0.99}{
  \subsection{NEW FEATURES}{
    \itemize{

      \item coverage,BamFile-method uses \code{yieldSize} to iterate
      through large files.

      \item coverage,character-method calculates coverage from a BAM
      file.

    }
  }
}
