CHANGES IN VERSION 1.10.0
------------------------

NEW FEATURES

    o add support for ##contig in VCF header 

    o add 'meta<-', 'info<-', 'geno<-' replacement methods for
      VCFHeader

    o add 'header<-' replacement method for VCF 

    o add strand to output from locationVariants() 

    o add support for writeVcf() to process Rle data in geno matrix 

    o readVcf() now parses 'geno' fields with Number=G as
      ((#alleles + 1) * (#alleles + 2)) / 2

    o writeVcf() now sorts the VCF when 'index=TRUE' 

    o add 'fixed<-,VCFHeader,DataFrameList' method 

    o add convenience functions for reading VCF into VRanges 

    o add Rplinkseq test script 

    o add 'isSNV', 'isInsertion', 'isDeletion', 'isIndel',
      'isTransition', 'isPrecise', 'isSV' and 'isSubstitution'
      generics 

    o add 'isSNV', 'isInsertion', 'isDeletion', 'isIndel'
      methods for VRanges and VCF classes

    o add match methods between ExpandedVCF and VRanges

    o add support for VRanges %in% TabixFile

MODIFICATIONS

    o expand,VCF-method ignores 'AD' header of 'AD' geno is NULL

    o add support for SIFT.Hsapiens.dbSNP137 

    o remove locateVariants() dependence on chr7-sub.vcf.gz 

    o modify expand() to handle 'AD' field where 'Number' is integer 

    o rename readVRangesFromVCF() to readVcfAsVRanges() 

    o remove check for circular chromosomes in locateVariants()
      and predictCoding() and refLocsToLocalLocs() 

    o modify filterVcf() to handle ranges in ScanVcfParam 

    o pass 'genetic.code' through predictCoding()

    o change default to 'row.names=TRUE' for readGT(), readGeno(),
      and readInfo() 

    o fixed() on empty VCF now returns DataFrame with column names
      and data types vs an empty DataFrame 

    o update biocViews 

    o modify 'show,VCF' to represent empty values in XStringSet 
      with '.'

    o replace rtracklayer:::pasteCollapse with unstrsplit()

DEPRECATED and DEFUNCT

    o remove defunct dbSNPFilter(), regionfilter() and MatrixToSnpMatrix() 

    o deprecate defunct readVcfLongForm() 

BUG FIXES

    o modify expand.geno() to handle case where header and geno don't match

    o modify writeVcf() to write out rownames with ":" character
      instead of treating as missing

    o fix how sample names were passed from 'ScanVcfParam' to scanVcf() 

    o fix bug in 'show,VCF' method

    o fix bugs in VRanges -> VCF coercion methods 

    o fix bug in lightweight read* functions that were ignoring
      samples in ScanVcfParam

    o fix bug in writeVcf() when no 'ALT' is present


CHANGES IN VERSION 1.8.0
------------------------

NEW FEATURES

    o Add 'upstream' and 'downstream' arguments to IntergenicVariants()
      constructor. 

    o Add 'samples' argument to ScanVcfParam(). 

    o Add readGT(), readGeno() and readInfo().

    o Add VRanges, VRangesList, SimpleVRangesList, and CompressedVRangesList 
      classes.

    o Add coercion VRanges -> VCF and VCF -> VRanges.

    o Add methods for VRanges family:
      altDepth(), refDepth(), totalDepth(), altFraction()
      called(), hardFilters(), sampleNames(), softFilterMatrix()
      isIndel(), resetFilter().

    o Add stackedSamples,VRangesList method.

MODIFICATIONS

    o VCF validity method now requires the number of rows in info()
      to match the length of rowData(). 

    o PRECEDEID and FOLLOWID from locateVariants() are now CharacterLists
      with all genes in 'upstream' and 'downstream' range. 

    o Modify rownames on rowData() GRanges to CHRAM:POS_REF/ALT for
      variants with no ID. 

    o readVcf() returns info() and geno() in the order specified in
      the ScanVcfParam. 

    o Work on scanVcf():
      - free parse memory at first opportunity
      - define it_next in .c rather than .h
      - parse ALT "." in C
      - hash incoming strings
      - parse only param-requested 'fixed', 'info', 'geno' fields

    o Add dimnames<-,VCF method to prevent 'fixed' fields from being
      copied into 'rowData' when new rownames or colnames were assigned. 

    o Support read/write for an emtpy VCF. 

    o readVcf(file=character, ...) method attempts coercion to
      TabixFile.

    o Support for read/write an emtpy VCF. 

    o Add performance section to vignette; convert to BiocStyle.

    o expand,CompressedVcf method expands geno() field 'AD' to
      length ALT + 1. The expanded field is a (n x y x 2) array. 
 
    o 'genome' argument to readVcf() can be a character(1) or
      Seqinfo object.

DEPRECATED and DEFUNCT

    o Defunct dbSNPFilter(), regionFilter() and MatrixToSnpMatrix().

    o Deprecate readVcfLongForm(). 

BUG FIXES

    o Fix bug in compatibility of read/writeVcf() when no INFO are columns
      present. 

    o Fix bug in locateVariants() when 'features' has no txid and cdsid. 

    o Fix bug in asVCF() when writing header lines.

    o Fix bug in "expand" methods for VCF to handle multiple 'A'
      columns in info().


CHANGES IN VERSION 1.6.0
------------------------

NEW FEATURES

    o VCF is now VIRTUAL. Concrete subclasses are CollapsedVCF 
      and ExpandedVCF. 

    o Add filterVcf() generic and methods for character and TabixFile. 
      This method creates one VCF file from another, using FilterRules.

    o Enhance show,VCF method with header information.
 
    o Stephanie Gogarten added genotypeToSnpMatrix() generic and 
      CollapsedVCF and matrix methods.

    o Chris Wallace added snpSummary() generic and CollapsedVCF
      method.

    o Add cbind and rbind for VCF objects. 

MODIFICATIONS

    o writeVcf,connection-method allows writing to console and appending.

    o writeVcf,connection-method accepts connections with open="a",
      only adding a header if the file does not already exist.

    o predictCoding and genotypeToSnpMatrix can now handle
      ALT as CharacterList. Structural variants are set to
      empty character ("").
 
    o When no INFO data are present in a vcf file, the info() 
      slot is now an empty DataFrame. Previously an empty column 
      named 'INFO' was returned. 

    o Empty VCF class now has an empty VCFHeader 

    o expand,CollapsedVCF-method expands 'geno' data with Number=A. 

    o VCF class accessors "fixed", "info" now return DataFrame instead
       of GRanges. "rowData" returns fixed fileds as the mcols. 

    o Updates to the vignette. 

DEPRECATED and DEFUNCT

    o Deprecate dbSNPFilter() and regionFilter().

    o Deprecate MatrixToSnpMatrix().

BUG FIXES

    o Multiple bugs fixed in "locateVariants".

    o Multiple bugs fixed in "writeVcf".

    o Bug fixed in subsetting of VCF objects. 

    o Bug fixed in "predictCoding" related to QUERYID column not
      mapping back to original indices (rows). 


CHANGES IN VERSION 1.4.0
------------------------

NEW FEATURES

    o "summarizeVariants" for summarizing counts by sample

    o new VariantType 'PromoterVariants()' added to "locateVariants"

MODIFICATIONS

    o "ref", "alt", "filt" and "qual" accessors for VCF-class now return
      a single variable instead of GRanges with variable as metadata 


CHANGES IN VERSION 1.2.0
------------------------

NEW FEATURES

    o "readVcf" has genome argument, can be subset on ranges or VCF elements
      with "ScanVcfParam"

    o "scanVcfHeader" returns VCFHeader class with accessors fixed, info, geno,
      etc.

    o "writeVcf" writes out a VCF file from a VCF class

    o "locateVariants" methods
        - returns GRanges instead of DataFrame
        - 'region' argument allows specification of variants by region 
        - output includes txID, geneID and cdsID
        - has cache argument for repeated calls over multiple vcf files

    o "predictCoding" methods
        - returns GRanges instead of DataFrame
        - output includes txID, geneID, cdsID, 
          cds-based and protein-based coordinates
 

CHANGES IN VERSION 1.0.0
------------------------

NEW FEATURES

    o "readVcf" methods for reading and parsing VCF files into a SummarizedExperiment 

    o "locateVariants" and "predictCoding" for identifying amino acid coding
      changes in nonsynonymous variants
 
    o "dbSNPFilter" and "regionFilter" for filtering variants on membership in
      dbSNP or on a particular location in the genome
 
    o access to PolyPhen and SIFT predictions through "keys" , "cols" and
      "select" methods. See ?SIFT or ?PolyPhen.

BUG FIXES

    o No changes classified as 'bug fixes' (package under active
    development)
