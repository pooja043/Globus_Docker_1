ó
·ÝáVc           @   sz   d  d l  Z  d  d l Z d d l m Z d  d l Z e e d d  Z d d d d d e d d e d d e e d  Z	 d S(   iÿÿÿÿNi   (   t   helpersc	         C   sº   | s |  }	 n= |  j  d | |  }
 | rF |
 j   }	 t j |
  n |
 }	 t |	 |  | |   } | rz t j |	  n  | r² | |  } t | t j  r® t j |  n  | S| Sd S(   sº   
    Given a BedTool object `orig_bedtool`, call its `method` with `args` and
    `kwargs` and then call `reduce_func` on the results.

    See parallel_apply docstring for details

    t   gN(   t   shufflet   sortR    t   close_or_deletet   getattrt
   isinstancet
   pybedtoolst   BedTool(   t   orig_bedtoolt   shuffle_kwargst	   genome_fnt   methodt   method_argst   method_kwargsR   R   t   reduce_funct   to_uset   shuffledt   resultt   reduced(    (    se   /usr/local/lib/python2.7/dist-packages/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/parallel.pyt   _parallel_wrap   s     	iè  c         #   sð  | p	 i  } | p d } t  | t  rQ t  | t  rQ t d t |    n  | pZ i  } | rx | rx t d   n   r® | s® | s t d   n  t j |  } q® n  t d |  d | d | d | d | d	 | d
  d | d |
  	}    f d   } |	 d k r?x( t |  D] } t	 | | |    VqWt
  n  | rN| } n t j |	  } g  t |  D]$ } | j t	 d | | |   ^ qj} xO t |  D]A \ } } | j   V| r¡t j j d |  t j j   q¡q¡Wt
  d S(   sp  
    Call an arbitrary BedTool method many times in parallel.

    An example use-case is to generate a null distribution of intersections,
    and then compare this to the actual intersections.

    **Important:** due to a known file handle leak in BedTool.__len__, it's
    best to simply check the number of lines in the file, as in the below
    function. This works because BEDTools programs strip any non-interval lines
    in the results.

    >>> # set up example BedTools
    >>> a = pybedtools.example_bedtool('a.bed')
    >>> b = pybedtools.example_bedtool('b.bed')

    >>> # Method of `a` to call:
    >>> method = 'intersect'

    >>> # Kwargs provided to `a.intersect` each iteration
    >>> method_kwargs = dict(b=b, u=True)

    >>> # Function that will be called on the results of
    >>> # `a.intersect(**method_kwargs)`.
    >>> def reduce_func(x):
    ...     return sum(1 for _ in open(x.fn))

    >>> # Create a small artificial genome for this test (generally you'd
    >>> # use an assembly name, like "hg19"):
    >>> genome = dict(chr1=(0, 1000))

    >>> # Do 10 iterations using 1 process for this test (generally you'd
    >>> # use 1000+ iterations, and as many processes as you have CPUs)
    >>> results = pybedtools.parallel.parallel_apply(a, method, genome=genome,
    ... method_kwargs=method_kwargs, iterations=10, processes=1,
    ... reduce_func=reduce_func, debug=True, report_iterations=True)

    >>> # get results
    >>> print(list(results))
    [2, 2, 3, 0, 3, 3, 0, 0, 2, 4]

    >>> # We can compare this to the actual intersection:
    >>> reduce_func(a.intersect(**method_kwargs))
    3

    Alternatively, we could use the `a.jaccard` method, which already does the
    reduction to a dictionary.  However, the Jaccard method requires the input
    to be sorted.  Here, we specify `sort=True` to sort each shuffled BedTool
    before calling its `jaccard` method.

    >>> from pybedtools.parallel import parallel_apply
    >>> a = pybedtools.example_bedtool('a.bed')
    >>> results = parallel_apply(a, method='jaccard', method_args=(b,),
    ... genome=genome, iterations=3, processes=1, sort=True, debug=True)
    >>> for i in results:
    ...     print(sorted(i.items()))
    [('intersection', 15), ('jaccard', 0.0238095), ('n_intersections', 2), ('union-intersection', 630)]
    [('intersection', 15), ('jaccard', 0.0238095), ('n_intersections', 2), ('union-intersection', 630)]
    [('intersection', 45), ('jaccard', 0.0818182), ('n_intersections', 1), ('union-intersection', 550)]

    Parameters
    ----------
    orig_bedtool : BedTool

    method : str
        The method of `orig_bedtool` to run

    method_args : tuple
        Passed directly to getattr(orig_bedtool, method)()

    method_kwargs : dict
        Passed directly to getattr(orig_bedtool, method)()

    shuffle : bool
        If True, then `orig_bedtool` will be shuffled at each iteration and
        that shuffled version's `method` will be called with `method_args` and
        `method_kwargs`.

    shuffle_kwargs : dict
        If `shuffle` is True, these are passed to `orig_bedtool.shuffle()`.
        You do not need to pass the genome here; that's handled separately by
        the `genome` and `genome_fn` kwargs.

    iterations : int
        Number of iterations to perform

    genome : string or dict
        If string, then assume it is the assembly name (e.g., hg19) and get
        a dictionary of chromsizes for that assembly, then converts to
        a filename.

    genome_fn : str
        Mutually exclusive with `genome`; `genome_fn` must be an existing
        filename with the chromsizes.  Use the `genome` kwarg instead if you'd
        rather supply an assembly or dict.

    reduce_func : callable
        Function or other callable object that accepts, as its only argument,
        the results from `orig_bedtool.method()`.  For example, if you care
        about the number of results, then you can use `reduce_func=len`.

    processes : int
        Number of processes to run.  If `processes=1`, then multiprocessing is
        not used (making it much easier to debug).  This argument is ignored if
        `_orig_pool` is provided.

    sort : bool
        If both `shuffle` and `sort` are True, then the shuffled BedTool will
        then be sorted.  Use this if `method` requires sorted input.

    _orig_pool : multiprocessing.Pool instance
        If provided, uses `_orig_pool` instead of creating one.  In this case,
        `processes` will be ignored.

    debug : bool
        If True, then use the current iteration index as the seed to shuffle.

    report_iterations : bool
        If True, then report the number of iterations to stderr.
    s+   method_args must be a list or tuple, got %ss1   only of of genome_fn or genome should be provideds<   shuffle=True, so either genome_fn or genome must be providedR	   R
   R   R   R   R   R   R   R   c            s!     r  r |  | d d <n  | S(   NR
   t   seed(    (   t   it   kwargs(   t   debugR   (    se   /usr/local/lib/python2.7/dist-packages/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/parallel.pyt   add_seedÅ   s    i   s   %sN(    (    (   R   t   listt   tuplet
   ValueErrort   typeR   t   chromsizes_to_filet   dictt   rangeR   t   StopIterationt   multiprocessingt   Poolt   apply_asynct	   enumeratet   gett   syst   stderrt   writet   flush(   R	   R   t   genomeR   R   R   R
   R   R   t	   processesR   t
   _orig_poolt
   iterationsR   t   report_iterationst   _parallel_wrap_kwargsR   t   itt   pt   resultsR   t   r(    (   R   R   se   /usr/local/lib/python2.7/dist-packages/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/parallel.pyt   parallel_apply+   sN    } 			4(
   R'   R"   t    R    R   t   Falset   Truet   NoneR   R5   (    (    (    se   /usr/local/lib/python2.7/dist-packages/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/parallel.pyt   <module>   s   "		