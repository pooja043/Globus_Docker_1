
## ----style, eval=TRUE, echo=FALSE, results="asis"------------------------
BiocStyle::latex()


## ----BiocParallel--------------------------------------------------------
library(BiocParallel)


## ----BiocParallelParam-SerialParam---------------------------------------
serialParam <- SerialParam()
serialParam


## ----BiocParallelParam-MulticoreParam------------------------------------
multicoreParam <- MulticoreParam(workers=8)
multicoreParam


## ----register------------------------------------------------------------
register(multicoreParam)


## ----registered----------------------------------------------------------
registered()


## ----devel-bplapply------------------------------------------------------
system.time(x <- bplapply(1:3, function(i) { Sys.sleep(i); i }))
unlist(x)


