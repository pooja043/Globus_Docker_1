## ----style, eval=TRUE, echo=FALSE, results='asis'---------------------------------------
BiocStyle::latex()

## ----loadGenomicFeatures----------------------------------------------------------------
library("GenomicFeatures")

## ----loadDb-----------------------------------------------------------------------------
samplefile <- system.file("extdata", "UCSC_knownGene_sample.sqlite",
                          package="GenomicFeatures")
txdb <- loadDb(samplefile)
txdb

## ----loadPackage------------------------------------------------------------------------
library(TxDb.Hsapiens.UCSC.hg19.knownGene)
txdb <- TxDb.Hsapiens.UCSC.hg19.knownGene #shorthand (for convenience)
txdb

## ----seqlevels--------------------------------------------------------------------------
head(seqlevels(txdb))

## ----seqlevels2-------------------------------------------------------------------------
seqlevels(txdb, force=TRUE) <- c("chr1")

## ----seqlevels3-------------------------------------------------------------------------
txdb <- restoreSeqlevels(txdb)

## ----seqlevels4-------------------------------------------------------------------------
seqlevels(txdb, force=TRUE) <- c("chr15")

## ----selectExample----------------------------------------------------------------------
keys <- c("100033416", "100033417", "100033420")
columns(txdb)
keytypes(txdb)
select(txdb, keys = keys, columns="TXNAME", keytype="GENEID")

## ----selectExercise---------------------------------------------------------------------
columns(txdb)
cols <- c("TXNAME", "TXSTRAND", "TXCHROM")
select(txdb, keys=keys, columns=cols, keytype="GENEID")

## ----transcripts1-----------------------------------------------------------------------
GR <- transcripts(txdb)
GR[1:3]

## ----transcripts2-----------------------------------------------------------------------
GR <- transcripts(txdb, vals <- list(tx_chrom = "chr15", tx_strand = "+"))
length(GR)
unique(strand(GR))

## ----exonsExer1-------------------------------------------------------------------------
EX <- exons(txdb)
EX[1:4]
length(EX)
length(GR)

## ----transcriptsBy----------------------------------------------------------------------
GRList <- transcriptsBy(txdb, by = "gene")
length(GRList)
names(GRList)[10:13]
GRList[11:12]

## ----exonsBy----------------------------------------------------------------------------
GRList <- exonsBy(txdb, by = "tx")
length(GRList)
names(GRList)[10:13]
GRList[[12]]

## ----internalID-------------------------------------------------------------------------
GRList <- exonsBy(txdb, by = "tx")
tx_ids <- names(GRList)
head(select(txdb, keys=tx_ids, columns="TXNAME", keytype="TXID"))

## ----introns-UTRs-----------------------------------------------------------------------
length(intronsByTranscript(txdb))
length(fiveUTRsByTranscript(txdb))
length(threeUTRsByTranscript(txdb))

## ----extract----------------------------------------------------------------------------
library(BSgenome.Hsapiens.UCSC.hg19)
tx_seqs1 <- extractTranscriptsFromGenome(Hsapiens, 
                                         TxDb.Hsapiens.UCSC.hg19.knownGene)

## ----translate1-------------------------------------------------------------------------
suppressWarnings(translate(tx_seqs1))

## ----betterTranslation------------------------------------------------------------------
cds_seqs <- extractTranscriptsFromGenome(Hsapiens, cdsBy(txdb, by="tx"))
translate(cds_seqs)

## ----supportedUCSCtables----------------------------------------------------------------
supportedUCSCtables()[1:4, ]

## ----makeTranscriptDbFromUCSC, eval=FALSE-----------------------------------------------
#  mm9KG <- makeTranscriptDbFromUCSC(genome = "mm9", tablename = "knownGene")

## ----discoverChromNames-----------------------------------------------------------------
head(getChromInfoFromUCSC("hg19"))

## ----makeTranscriptDbFromBiomart, eval=FALSE--------------------------------------------
#  mmusculusEnsembl <-
#     makeTranscriptDbFromBiomart(biomart = "ensembl",
#                                dataset = "mmusculus_gene_ensembl")

## ----saveFeatures 1, eval=FALSE---------------------------------------------------------
#  saveDb(mm9KG, file="fileName.sqlite")

## ----loadFeatures-1, eval=FALSE---------------------------------------------------------
#  mm9KG <- loadDb("fileName.sqlite")

## ----SessionInfo, echo=FALSE------------------------------------------------------------
sessionInfo()

