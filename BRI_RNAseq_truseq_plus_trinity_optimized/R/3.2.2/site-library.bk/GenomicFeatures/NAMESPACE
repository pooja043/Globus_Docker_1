import(methods)

importFrom(utils, download.file, installed.packages, read.table)

import(BiocGenerics)
import(IRanges)
import(GenomicRanges)

importFrom(GenomeInfoDb, rankSeqlevels, seqlevelsStyle)

importMethodsFrom(DBI, dbCommit, dbConnect,
                  dbDisconnect, dbExistsTable, dbGetQuery,
                  dbReadTable, dbWriteTable, dbListTables, dbListFields)

importFrom(RSQLite, SQLite, SQLITE_RO)
importMethodsFrom(RSQLite, dbBeginTransaction, dbGetPreparedQuery)

importFrom(RCurl, getURL)

importFrom(Biobase, createPackage)

importClassesFrom(Biostrings, DNAString, DNAStringSet, MaskedDNAString)
importFrom(Biostrings, "masks<-")

importFrom(biomaRt, getBM, listAttributes, listDatasets, listMarts,
           useDataset, useMart)

importFrom(rtracklayer, browserSession, ucscTableQuery,
           tableNames, getTable, trackNames, ucscSchema, asBED, asGFF, import)

importFrom(AnnotationDbi, loadDb, saveDb)

exportClasses(TranscriptDb, FeatureDb)

export(
  loadFeatures,
  transcripts_deprecated,
  exons_deprecated,
  introns_deprecated,
  DEFAULT_CIRC_SEQS,
  makeTranscriptDb,
  supportedUCSCtables,
  supportedUCSCFeatureDbTracks,
  supportedUCSCFeatureDbTables,
  UCSCFeatureDbTableSchema,
  getChromInfoFromUCSC,
  makeTranscriptDbFromUCSC,
  getChromInfoFromBiomart,
  makeTranscriptDbFromBiomart,
  makeTranscriptDbFromGFF,
  transcripts, exons, cds, genes,
  disjointExons,
  microRNAs,
  tRNAs,
  transcriptsByOverlaps,
  exonsByOverlaps,
  cdsByOverlaps,
  id2name,
  transcriptsBy,
  exonsBy,
  cdsBy,
  intronsByTranscript,
  fiveUTRsByTranscript,
  threeUTRsByTranscript,
  extractTranscriptSeqs,
  transcriptWidths,
  transcriptLocs2refLocs,
  extractTranscripts,
  extractTranscriptsFromGenome,
  sortExonsByRank,
  makeFeatureDbFromUCSC,
  makeTxDbPackage,
  makeTxDbPackageFromUCSC,
  makeTxDbPackageFromBiomart,
  makeFDbPackageFromUCSC,	
  features,
  supportedMiRBaseBuildValues,
  getPromoterSeq,
  ## Deprecated
  determineDefaultSeqnameStyle
)

exportMethods(
  saveFeatures,
  metadata,
  show,
  as.list,
  seqinfo, "seqinfo<-",
  transcripts, exons, cds, genes,
  promoters,
  disjointExons,
  microRNAs,
  tRNAs,
  transcriptsByOverlaps,
  exonsByOverlaps,
  cdsByOverlaps,
  transcriptsBy,
  exonsBy,
  cdsBy,
  intronsByTranscript,
  fiveUTRsByTranscript,
  threeUTRsByTranscript,
  extractTranscriptSeqs,
  isActiveSeq,
  "isActiveSeq<-",
  asBED, asGFF,
  getPromoterSeq,
  distance
)
