ó
cTc           @   s¥   d  Z  d d l Z d d l Z d d l Z d d l m Z d d l Z d d l m Z e j d  Z	 d   Z
 d   Z d   Z d	   Z d d d d e d
  Z d S(   sÖ   
Interface between pybedtools and the R package VennDiagram.

Rather than depend on the user to have rpy2 installed, this simply writes an
R script that can be edited and tweaked by the user before being run in R.
iÿÿÿÿN(   t   helpers(   t   OrderedDictsb   
library(VennDiagram)
venn.diagram(
    x=$x,
    filename=$filename,
    category.names = $names
c         C   sf   g  } xL |  D]D } t  | t j  r@ t |  j d d  } n  | j d |  q Wd d j |  S(   s²   
    Convert items in `x` to a string, and replace tabs with pipes in Interval
    string representations.  Put everything into an R vector and return as one
    big string.
    s   	t   |s   "%s"s   c(%s)t   ,(   t
   isinstancet
   pybedtoolst   Intervalt   strt   replacet   appendt   join(   t   xt   itemst   i(    (    s~   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.6.6-py2.7-linux-x86_64.egg/pybedtools/contrib/venn_maker.pyt   _list_to_R_syntax   s    c         C   sQ   g  } x7 |  j    D]) \ } } | j d | t |  f  q Wd d j |  S(   sI   
    Calls _list_to_R_syntax for each item.  Returns one big string.
    s	   "%s" = %ss   list(%s)s   , (   R   R	   R   R
   (   t   dR   t   keyt   val(    (    s~   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.6.6-py2.7-linux-x86_64.egg/pybedtools/contrib/venn_maker.pyt   _dict_to_R_named_list)   s    !c         C   s+   t  j |  j t |  j  t |  j  g  S(   s=   
    Convert a feature of any format into a BED3 format.
    (   R   t   create_interval_from_listt   chromR   t   startt   stop(   t   feature(    (    s~   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.6.6-py2.7-linux-x86_64.egg/pybedtools/contrib/venn_maker.pyt	   truncator3   s    c         C   s$  t  |   d k re |  d j t  j   } |  d j t  j   } | | j | |  } | | f St  |   d k r|  d j t  j   } |  d j t  j   } |  d j t  j   } | | j | |  } | | | j | |  j | | |  } | | | f St  |   d k r |  d j t  j   } |  d j t  j   } |  d j t  j   } |  d j t  j   } | | j | |  } | | | j | |  j | | |  } | | | | j | |  j | | |  j | | | |  } | | | | f Sd S(   st  
    Perform interval intersections such that the end products have identical     features for overlapping intervals.

    The VennDiagram package does *set* intersection, not *interval*
    intersection.  So the goal here is to represent intersecting intervals as
    intersecting sets of strings.

    Doing a simple BEDTools intersectBed call doesn't do the trick (even with
    the -u argument).  As a concrete example, what would the string be for an
    intersection of the feature "chr1:1-100" in file `x` and "chr1:50-200" in
    file `y`?

    The method used here is to substitute the intervals in `y` that overlap `x`
    with the corresponding elements in `x`.  This means that in the resulting
    sets, the overlapping features are identical.  To follow up with the
    example, both `x` and `y` would have an item "chr1:50-200" in their sets,
    simply indicating *that* one interval overlapped.

    Venn diagrams are not well suited for nested overlaps or multi-overlaps.
    To illustrate, try drawing the 2-way Venn diagram of the following two
    files. Specifically, what number goes in the middle -- the number of
    features in `x` that intersect `y` (1) or the number of features in `y`
    that intersect `x` (2)?::

        x:
            chr1  1  100
            chr1 500 6000

        y:
            chr1 50 100
            chr1 80 200
            chr9 777 888

    In this case, this function will return the following sets::

        x:
            chr1:1-100
            chr1:500-6000

        y:
            chr1:1-100
            chr9:777-888

    This means that while `x` does not change in length, `y` can.  For example,
    if there are 2 features in `x` that overlap one feature in `y`, then `y`
    will gain those two features in place of its single original feature.

    This strategy is extended for multiple intersections -- see the source for
    details.
    i   i    i   i   i   N(   t   lent   eachR   t   saveast   cat(   R   R   t   yt   new_yt   zt   new_zt   qt   new_q(    (    s~   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.6.6-py2.7-linux-x86_64.egg/pybedtools/contrib/venn_maker.pyt   cleaned_intersect;   s0    4
,,c         C   s÷  | d k r d } n
 d | } | d k r> d t |    } n  g  } x? |  D]7 } t | t j  su t j |  } n  | j |  qK Wt |  } t t | |   }	 t	 j
 d t |	  d | d t |   }
 | rñ |
 d d j |  7}
 n  |
 d	 7}
 | st j j   } n | } t | d
  } | j |
  | j   | d } | rét j j sht j   n  t j j t j j d  d d | | g } t j | d t j d t j } | j   \ } } | sÑ| réd G| GHd G| GHqén  | só|
 Sd S(   s  
    Given a list of interval files, write an R script to create a Venn     diagram of overlaps (and optionally run it).

    The R script calls the venn.diagram function of the R package VennDiagram
    for extremely flexible Venn and Euler diagram creation.  Uses
    `cleaned_intersect()` to create string representations of shared intervals.

    `beds` is a list of up to 4 filenames or BedTools.

    `names` is a list of names to use for the Venn diagram, in the same order
    as `beds`. Default is "abcd"[:len(beds)].

    `figure_filename` is the TIFF file to save the figure as.

    `script_filename` is the optional filename to write the R script to

    `additional_args` is list that will be inserted into the R script,
    verbatim.  For example, to use scaled Euler diagrams with different colors,
    use::

        additional_args = ['euler.d=TRUE',
                           'scaled=TRUE',
                           'cat.col=c("red","blue")']

    If `run` is True, then assume R is installed, is on the path, and has
    VennDiagram installed . . . and run the script.  The resulting filename
    will be saved as `figure_filename`.
    t   NULLs   "%s"t   abcdR   t   filenamet   namesR   s   , t   )t   ws   .Routt   Rt   CMDt   BATCHt   stdoutt   stderrs   stdout:s   stderr:N(   t   NoneR   R   R   t   BedToolR	   R#   R   t   zipt   templatet
   substituteR   R   R
   t   _tmpt   opent   writet   closet   settingst   _R_installedR    t   _check_for_Rt   ost   patht   _R_patht
   subprocesst   Popent   PIPEt   communicate(   t   bedsR'   t   figure_filenamet   script_filenamet   additional_argst   runt   _bedst   bedt   cleanedt   resultst   st   fnt   foutt   outt   cmdst   pR-   R.   (    (    s~   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.6.6-py2.7-linux-x86_64.egg/pybedtools/contrib/venn_maker.pyt
   venn_maker¡   sP     	
	


	(   t   __doc__R;   t   stringR   R    R>   t   collectionsR   t   TemplateR2   R   R   R   R#   R/   t   FalseRQ   (    (    (    s~   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.6.6-py2.7-linux-x86_64.egg/pybedtools/contrib/venn_maker.pyt   <module>   s   			
		f	