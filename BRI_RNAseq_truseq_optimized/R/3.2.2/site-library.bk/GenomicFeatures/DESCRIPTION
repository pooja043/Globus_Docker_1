Package: GenomicFeatures
Title: Tools for making and manipulating transcript centric annotations
Version: 1.16.3
Author: M. Carlson, H. Pages, P. Aboyoun, S. Falcon, M. Morgan,
        D. Sarkar, M. Lawrence
License: Artistic-2.0
Description: A set of tools and methods for making and manipulating
        transcript centric annotations. With these tools the user can
        easily download the genomic locations of the transcripts, exons
        and cds of a given organism, from either the UCSC Genome
        Browser or a BioMart database (more sources will be supported
        in the future). This information is then stored in a local
        database that keeps track of the relationship between
        transcripts, exons, cds and genes. Flexible methods are
        provided for extracting the desired features in a convenient
        format.
Maintainer: Bioconductor Package Maintainer
 <maintainer@bioconductor.org>
Depends: BiocGenerics (>= 0.1.0), IRanges (>= 1.17.13), GenomicRanges
        (>= 1.15.25), AnnotationDbi (>= 1.25.7)
Imports: methods, DBI (>= 0.2-5), RSQLite (>= 0.8-1), Biostrings (>=
        2.23.2), rtracklayer (>= 1.15.1), biomaRt (>= 2.17.1), RCurl,
        utils, Biobase (>= 2.15.1), GenomeInfoDb
Suggests: org.Mm.eg.db, BSgenome, BSgenome.Hsapiens.UCSC.hg18 (>=
        1.3.14), BSgenome.Hsapiens.UCSC.hg19 (>= 1.3.17),
        BSgenome.Celegans.UCSC.ce2, BSgenome.Dmelanogaster.UCSC.dm3 (>=
        1.3.17), mirbase.db, FDb.UCSC.tRNAs,
        TxDb.Hsapiens.UCSC.hg18.knownGene,
        TxDb.Hsapiens.UCSC.hg19.knownGene,
        TxDb.Dmelanogaster.UCSC.dm3.ensGene (>= 2.7.1), Rsamtools,
        pasillaBamSubset (>= 0.0.5), seqnames.db, RUnit, BiocStyle,
        knitr
Collate: utils.R Ensembl.utils.R TranscriptDb-class.R FeatureDb-class.R
        makeTranscriptDb.R makeTranscriptDbFromUCSC.R
        makeTranscriptDbFromBiomart.R makeTranscriptDbFromGFF.R
        makeFeatureDbFromUCSC.R saveFeatures.R id2name.R transcripts.R
        transcriptsByOverlaps.R transcriptsBy.R regions.R features.R
        extractTranscriptsFromGenome.R extractTranscriptSeqs.R
        makeTxDbPackage.R seqnames-methods.R select-methods.R
        nearest-methods.R getPromoterSeq-methods.R
        test_GenomicFeatures_package.R
VignetteBuilder: knitr
biocViews: Genetics, Infrastructure, Annotation, Sequencing
Packaged: 2014-10-02 02:49:17 UTC; biocbuild
Built: R 3.2.2; ; 2015-08-19 18:14:17 UTC; unix
