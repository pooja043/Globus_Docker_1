useDynLib(GenomicRanges)

import(methods)
import(utils)  # for as.roman()
importFrom(stats, setNames)
import(BiocGenerics)
import(IRanges)
import(XVector)  # only for the "Views" method for integer vectors, the
                 # XIntegerViews class, and the "viewMins", "viewMaxs", and
                 # "viewSums" methods for XIntegerViews objects
import(GenomeInfoDb)

exportClasses(
    Constraint, ConstraintORNULL,
    Seqinfo,
    GenomicRanges, GenomicRangesORmissing, GRanges, GRangesList, GIntervalTree,
    GenomicRangesORGRangesList,
    SummarizedExperiment, Assays,
    GenomicRangesList, SimpleGenomicRangesList
)


### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
### Export S3 methods
###

S3method(duplicated, GenomicRanges)

S3method(sort, GenomicRanges)

### We also export them thru the export() directive so that (a) they can be
### called directly, (b) tab-completion on the name of the generic shows them,
### and (c) methods() doesn't asterisk them.
export(
    duplicated.GenomicRanges,

    sort.GenomicRanges
)



### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
### Export S4 methods for generics not defined in GenomicRanges
###

exportMethods(
    length,
    names, "names<-",
    "dimnames<-",
    "[", "[<-", "[[", "[[<-", "$", "$<-",
    as.data.frame,
    coerce,
    c,
    cbind,
    rbind,
    show,
    "==", "<=",
    duplicated, match,
    order, sort, rank,
    union, intersect, setdiff,
    start, end,
    split, unlist,
    range,
    Ops,
    merge,
    updateObject,
    strand, "strand<-",

    ## Generics defined in IRanges:
    elementMetadata, "elementMetadata<-",
    mcols, "mcols<-",
    values, "values<-",
    relistToClass, splitAsListReturnedClass,
    width, "start<-", "width<-", "end<-",
    compare,
    ranges, "ranges<-",
    ngap,
    score, "score<-",
    shift, narrow, flank, resize, promoters, restrict,
    reduce, gaps, disjoin, isDisjoint, disjointBins,
    coverage,
    punion, pintersect, psetdiff, pgap,
    findOverlaps, countOverlaps, overlapsAny, subsetByOverlaps,
    precede, follow, nearest, distance, distanceToNearest,
    map,
    tile,
    subset,

    ## Generics defined in GenomeInfoDb:
    seqlevelsStyle
)


### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
### Export non-generic functions
###

export(
    phicoef,

    makeSeqnameIds,
    Seqinfo,
    GRanges, GRangesList, GenomicRangesList, GIntervalTree, 
    .DollarNames.GenomicRanges,
    makeGRangesFromDataFrame,
    makeGRangesListFromFeatureFragments,
    tileGenome,

    keepSeqlevels, dropSeqlevels, renameSeqlevels, restoreSeqlevels,
    keepStandardChromosomes
)


### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
### Export S4 generics defined in GenomicRanges + export corresponding
### methods
###

export(
    ## constraint.R:
    #constraint, "constraint<-",
    checkConstraint,

    ## seqinfo.R:
    seqinfo, "seqinfo<-",
    seqnames, "seqnames<-",
    seqlevels, "seqlevels<-",
    sortSeqlevels,
    seqlevelsInUse,
    seqlevels0,
    seqlengths, "seqlengths<-",
    isCircular, "isCircular<-",
    genome, "genome<-",
    seqnameStyle, "seqnameStyle<-",

    ## range-squeezers.R:
    granges, grglist, rglist,

    ## SummarizedExperiment-class.R:
    SummarizedExperiment,
    exptData, "exptData<-",
    rowData, "rowData<-",
    colData, "colData<-",
    assays, "assays<-",
    assay, "assay<-"
)

### Exactly the same list as above.
exportMethods(
    #constraint, "constraint<-",
    checkConstraint,
    seqinfo, "seqinfo<-",
    seqnames, "seqnames<-",
    seqlevels, "seqlevels<-",
    sortSeqlevels,
    seqlevelsInUse,
    seqlengths, "seqlengths<-",
    isCircular, "isCircular<-",
    genome, "genome<-",
    seqnameStyle, "seqnameStyle<-",
    granges, grglist, rglist,
    SummarizedExperiment,
    exptData, "exptData<-",
    rowData, "rowData<-",
    colData, "colData<-",
    assays, "assays<-",
    assay, "assay<-"
)

