useDynLib(biovizBase, .registration=TRUE)

## ======================================================================
## Import
## ======================================================================
## methods
import(methods)
importFrom(stats, setNames)
## BiocGenerics
import(BiocGenerics)
importFrom(grDevices, colorRampPalette)

## importFrom(BiocGenerics, cbind, rbind)
## RColorBrewer
import(RColorBrewer)
import(dichromat)
## scales
import(scales)
## Hmisc
importFrom(Hmisc, bezier)
## IRanges
importFrom(IRanges, IRanges,  disjointBins,
           seqapply, breakInChunks,
           findOverlaps, subsetByOverlaps,
           "elementMetadata", "elementMetadata<-",
           metadata, "metadata<-",
           start, width, end,  resize, 
           "values", "values<-",
           ranges, "ranges<-", 
           isTRUEorFALSE, coverage, slice,
           elementLengths,endoapply,
           ## viewWhichMins, viewWhichMaxs,
           ## viewMins, viewMaxs, viewSums, viewMeans,
           ## runValue, runLength,
           reduce, punion, pgap, gaps)

importMethodsFrom(IRanges,  split, sort, as.factor,
                  unlist, restrict,
                  as.data.frame,
                  length,
                  "[","[<-","[[", "[[<-","$",
                  "%in%", show)


importClassesFrom(IRanges)

## ## GenomicRanges

importFrom(GenomicRanges, keepSeqlevels, renameSeqlevels,
           "seqnames<-","strand<-", "seqlevels<-", seqinfo,
           "seqinfo<-", seqlevels, GRanges, GRangesList, Seqinfo,
           seqlengths, "seqlengths<-")
importMethodsFrom(GenomicRanges, ranges, "ranges<-", start, end,
                  width, "start<-", "end<-", "width<-", seqnames,
                  strand, show, "elementMetadata<-", elementMetadata,
                  as.data.frame)

importClassesFrom(GenomicRanges, GRanges, GenomicRanges)

## Biostrings
importFrom(Biostrings, AA_ALPHABET,
           DNA_ALPHABET,
           DNA_BASES,
           IUPAC_CODE_MAP,
           RNA_ALPHABET,
           AMINO_ACID_CODE,           
           RNA_BASES)

## Rsamtools
importClassesFrom(Rsamtools, BamFile)
importFrom(Rsamtools, index, scanBamFlag, PileupFiles,
           PileupParam)
importMethodsFrom(Rsamtools, path, ScanBamParam,
                 applyPileups, scanBam, readGAlignmentsFromBam)

## GenomicFeatures
importClassesFrom(GenomicFeatures,TranscriptDb)
importFrom(GenomicFeatures, transcripts)
           
           
importMethodsFrom(GenomicFeatures, isActiveSeq, "isActiveSeq<-", exonsByOverlaps,
                  transcriptsBy, exonsBy, cdsBy)


## ======================================================================
##    export
## ======================================================================
## color
export(getBioColor,
       plotColorLegend,
       genBrewerBlindPalInfo,
       genDichromatPalInfo,
       genBlindPalInfo,
       colorBlindSafePal,
       blind.pal.info,
       brewer.pal.blind.info,
       dichromat.pal.blind.info,
       estimateCoverage)



## transform
export(transformToCircle,
       transformToRectInCircle,
       transformToBarInCircle,
       transformToSegInCircle,
       transformToLinkInCircle,
       transformDfToGr,
       transformGRangesForEvenSpace)
exportMethods(transformToGenome,transformToDf)

## utils
exportMethods(addStepping, 
              shrinkageFun,
              maxGap, 
              splitByFacets,
              getGaps,
              getXScale,
              getYLab,
              getXLab,
              crunch)

export(isIdeogram, isSimpleIdeogram, getIdeogram, containLetters,
       pileupAsGRanges, pileupGRangesAsVariantTable, GCcontent,
       showColor, isJunctionRead, isMatchedWithModel,
       flatGrl, getIdeoGR, getScale, getFormalNames, subsetArgsByFormals,
       parseArgsForAes, parseArgsForNonAes, strip_formula_dots,
       is_coord_truncate_gaps, is_coord_genome)


## exportPattern("^[^\\.]")
