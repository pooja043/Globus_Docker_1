### R code from vignette source 'create_objects.Rnw'

###################################################
### code chunk number 1: samples
###################################################
tab = data.frame(
  file = c("treated1fb", "treated2fb", "treated3fb", "untreated1fb", "untreated2fb", "untreated3fb", "untreated4fb"),
  type = c("single-read", "paired-end", "paired-end", "single-read", "single-read", "paired-end", "paired-end"),
  "number of lanes" = as.integer(c(5,2,2,2,6,2,2)),
  "total number of reads" = c("35158667", "12242535 (x2)", "12443664 (x2)", "17812866", "34284521", "10542625 (x2)", "12214974 (x 2)"),
  "exon counts" = as.integer(c(15679615, 15620018, 12733865, 14924838, 20764558, 10283129, 11653031)),
  stringsAsFactors = TRUE,
  check.names = FALSE)
library("xtable")
print(xtable(tab, caption = "Read numbers and alignment statistics. The column \\emph{exon counts} refers to the number of reads that could be uniquely aligned to an exon.", label="tab:samples"), file="create_objects_tabsamples.tex")


###################################################
### code chunk number 2: pasilla
###################################################
library("pasilla")


###################################################
### code chunk number 3: extdata
###################################################
inDir = system.file("extdata", package="pasilla", mustWork=TRUE)
dir(inDir)


###################################################
### code chunk number 4: samples1
###################################################
z = sub("fb$", "", tab$file)
nc = nchar(z)
samples = data.frame(
  condition = substr(z, 1, nc-1),
#  replicate = as.integer(substr(z, nc, nc)),
  type = tab$type,
  row.names = tab$file,
  stringsAsFactors = TRUE,
  check.names = FALSE)

#stopifnot(!any(is.na(samples$replicate)))


###################################################
### code chunk number 5: samples
###################################################
head(samples)


###################################################
### code chunk number 6: ecs
###################################################
annotationfile = file.path(inDir, "Dmel.BDGP5.25.62.DEXSeq.chr.gff")


###################################################
### code chunk number 7: read
###################################################
library("DEXSeq")
dxd = DEXSeqDataSetFromHTSeq(
  countfiles = file.path(inDir, paste(rownames(samples), "txt", sep=".")), 
  sampleData=samples,
  design= ~ sample + exon + condition:exon,
  flattenedfile = annotationfile)


###################################################
### code chunk number 8: gfs
###################################################
genesforsubset = readLines(file.path(inDir, "geneIDsinsubset.txt"))
dxd = dxd[geneIDs( dxd ) %in% genesforsubset,]


###################################################
### code chunk number 9: save (eval = FALSE)
###################################################
## save(dxd, file=file.path("..", "data", "pasillaDEXSeqDataSet.RData"))


###################################################
### code chunk number 10: sessionInfo
###################################################
toLatex(sessionInfo())


