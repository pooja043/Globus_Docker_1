Package: GenomicAlignments
Title: Representation and manipulation of short genomic alignments
Description: Provides efficient containers for storing and manipulating
	short genomic alignments (typically obtained by aligning short reads
	to a reference genome). This includes read counting, computing the
	coverage, junction detection, and working with the nucleotide content
	of the alignments.
Version: 1.0.6
Author: Herv\'e Pag\`es, Valerie Obenchain, Martin Morgan
Maintainer: Bioconductor Package Maintainer <maintainer@bioconductor.org>
biocViews: Genetics, Infrastructure, DataImport, Sequencing, RNASeq,
        SNP
Depends: R (>= 2.10), methods, BiocGenerics (>= 0.7.7), IRanges (>=
        1.21.25), GenomicRanges (>= 1.15.32), Biostrings (>= 2.31.10),
        Rsamtools (>= 1.15.26), BSgenome (>= 1.31.12)
Imports: methods, stats, BiocGenerics, IRanges, GenomicRanges,
        Biostrings, Rsamtools, BiocParallel
LinkingTo: IRanges
Suggests: rtracklayer, GenomicFeatures, RNAseqData.HNRNPC.bam.chr14,
        pasillaBamSubset, TxDb.Hsapiens.UCSC.hg19.knownGene,
        TxDb.Dmelanogaster.UCSC.dm3.ensGene,
        BSgenome.Dmelanogaster.UCSC.dm3, BSgenome.Hsapiens.UCSC.hg19,
        DESeq, edgeR, RUnit, BiocStyle
License: Artistic-2.0
Collate: utils.R cigar-utils.R GAlignments-class.R
        GAlignmentPairs-class.R GAlignmentsList-class.R
        GappedReads-class.R OverlapEncodings-class.R
        findMateAlignment.R readGAlignments.R junctions-methods.R
        sequenceLayer.R stackStringsFromBam.R pileLettersAt.R
        intra-range-methods.R coverage-methods.R setops-methods.R
        findOverlaps-methods.R map-methods.R encodeOverlaps-methods.R
        findCompatibleOverlaps-methods.R summarizeOverlaps-methods.R
        findSpliceOverlaps-methods.R zzz.R
Packaged: 2014-08-22 04:07:21 UTC; biocbuild
Built: R 3.2.2; x86_64-pc-linux-gnu; 2015-08-19 18:13:11 UTC; unix
