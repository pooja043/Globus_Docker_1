ó
ÛâTc           @   sï   d  Z  d Z d Z d d l Z d d l Z d d l Z d d l Z d d l j j	 Z
 d d l j j Z d d l j j Z d d l j Z d d l j Z d d l j Z d d l Z d Z d Z d Z d Z d	 Z  d
 Z! d e" f d     YZ# d S(   s   Andre Merzky, Ole Weidners%   Copyright 2012-2013, The SAGA Projectt   MITiÿÿÿÿNg       @i    i   i   i   i   t   PTYShellc           B   sÚ   e  Z d  Z d d d i  d  Z d   Z d   Z e d  Z e d  Z	 d   Z
 d d  Z d	   Z d d
  Z d d d  Z d   Z d   Z d   Z d   Z d d  Z d d  Z d d  Z d d  Z RS(   sê  
    This class wraps a shell process and runs it as a :class:`PTYProcess`.  The
    user of this class can start that shell, and run arbitrary commands on it.

    The shell to be run is expected to be POSIX compliant (bash, dash, sh, ksh
    etc.) -- in particular, we expect the following features:
    ``$?``,
    ``$!``,
    ``$#``,
    ``$*``,
    ``$@``,
    ``$$``,
    ``$PPID``,
    ``>&``,
    ``>>``,
    ``>``,
    ``<``,
    ``|``,
    ``||``,
    ``()``,
    ``&``,
    ``&&``,
    ``wait``,
    ``kill``,
    ``nohup``,
    ``shift``,
    ``export``,
    ``PS1``, and
    ``PS2``.

    Note that ``PTYShell`` will change the shell prompts (``PS1`` and ``PS2``),
    to simplify output parsing.  ``PS2`` will be empty, ``PS1`` will be set
    ``PROMPT-$?->`` -- that way, the prompt will report the exit value of the
    last command, saving an extra roundtrip.  Users of this class should be
    careful when setting other prompts -- see :func:`set_prompt` for more
    details.

    Usage Example::

        # start the shell, find its prompt.  
        self.shell = saga.utils.pty_shell.PTYShell ("ssh://user@remote.host.net/", contexts, self._logger)

        # run a simple shell command, merge stderr with stdout.  $$ is the pid
        # of the shell instance.
        ret, out, _ = self.shell.run_sync (" mkdir -p /tmp/data.$$/" )

        # check if mkdir reported success
        if  ret != 0 :
            raise saga.NoSuccess ("failed to prepare base dir (%s)(%s)" % (ret, out))

        # stage some data from a local string variable into a file on the remote system
        self.shell.stage_to_remote (src = pbs_job_script, 
                                    tgt = "/tmp/data.$$/job_1.pbs")

        # check size of staged script (this is actually done on PTYShell level
        # already, with no extra hop):
        ret, out, _ = self.shell.run_sync (" stat -c '%s' /tmp/data.$$/job_1.pbs" )
        if  ret != 0 :
            raise saga.NoSuccess ("failed to check size (%s)(%s)" % (ret, out))

        assert (len(pbs_job_script) == int(out))


    **Data Staging and Data Management:**
    

    The PTYShell class does not only support command execution, but also basic
    data management: for SSH based shells, it will create a tunneled scp/sftp
    connection for file staging.  Other data management operations (mkdir, size,
    list, ...) are executed either as shell commands, or on the scp/sftp channel
    (if possible on the data channel, to keep the shell pty free for concurrent
    command execution).  Ssh tunneling is implemented via ssh.v2 'ControlMaster'
    capabilities (see `ssh_config(5)`).
    
    For local shells, PTYShell will create an additional shell pty for data
    management operations.  


    **Asynchronous Notifications:**

    A third pty process will be created for asynchronous notifications.  For
    that purpose, the shell started on the first channel will create a named
    pipe, at::

      $HOME/.saga/adaptors/shell/async.$$

    ``$$`` here represents the pid of the shell process.  It will also set the
    environment variable ``SAGA_ASYNC_PIPE`` to point to that named pipe -- any
    application running on the remote host can write event messages to that
    pipe, which will be available on the local end (see below).  `PTYShell`
    leaves it unspecified what format those messages have, but messages are
    expected to be separated by newlines.
    
    An adaptor using `PTYShell` can subscribe for messages via::

      self.pty_shell.subscribe (callback)

    where callback is a Python callable.  PTYShell will listen on the event
    channel *in a separate thread* and invoke that callback on any received
    message, passing the message text (sans newline) to the callback.

    An example usage: the command channel may run the following command line::

      ( sh -c 'sleep 100 && echo "job $$ done" > $SAGA_ASYNC_PIPE"                          || echo "job $$ fail" > $SAGA_ASYNC_PIPE" ) &

    which will return immediately, and send a notification message at job
    completion.

    Note that writes to named pipes are not atomic.  From POSIX:

    ``A write is atomic if the whole amount written in one operation is not
    interleaved with data from any other process. This is useful when there are
    multiple writers sending data to a single reader. Applications need to know
    how large a write request can be expected to be performed atomically. This
    maximum is called {PIPE_BUF}. This volume of IEEE Std 1003.1-2001 does not
    say whether write requests for more than {PIPE_BUF} bytes are atomic, but
    requires that writes of {PIPE_BUF} or fewer bytes shall be atomic.`

    Thus the user is responsible for ensuring that either messages are smaller
    than *PIPE_BUF* bytes on the remote system (usually at least 1024, on Linux
    usually 4096), or to lock the pipe on larger writes.


    **Automated Restart, Timeouts:**

    For timeout and restart semantics, please see the documentation to the
    underlying :class:`saga.utils.pty_process.PTYProcess` class.

    c         C   s  | r | |  _  n t j d d  |  _  | r9 | |  _ n t j d t  |  _ |  j  j d |   | |  _ | |  _	 | |  _
 d |  _ d  |  _ t |  _ |  j j d  |  _ d |  j k rô |  j d j   |  _ t j d |  j t j  |  _ n( d	 |  _ t j d |  j t j  |  _ |  j  j d
 |  j  t j d d |  _ y t j |  j  WnP t k
 r­} | j t j k rt j  j! |  j  rq®t" j# d |   n Xt$ j%   |  _& |  j& j' |  j |  j |  j |  j   |  _( |  j& j) |  j(  |  _* |  j'   d  S(   Nt   sagaR   t   defaults   PTYShell init %sg        s   saga.utils.ptyt   prompt_patterns   ^(.*?)%ss   [\$#%>\]]\s*$s   PTY prompt pattern: %st   HOMEs   /.saga/adaptors/shell/s    could not create staging dir: %s(+   t   loggert   rult	   getLoggert   sessiont   sst   Sessiont   Truet   debugt   urlt   initt   optst   latencyt   Nonet   cp_slavet   Falset   initializedt
   get_configt   cfgt	   get_valuet   promptt   ret   compilet   DOTALLt	   prompt_ret   infot   ost   environt   baset   makedirst   OSErrort   errnot   EEXISTt   patht   isdirt   set	   NoSuccesst   supsft   PTYShellFactoryt   factoryt
   initializet   pty_infot	   run_shellt	   pty_shell(   t   selfR   R	   R   R   R   t   e(    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyt   __init__´   s>      						"	'*c         C   s(   |  j  j d |   |  j d t  d  S(   Ns   PTYShell del  %st   kill_pty(   R   R   t   finalizeR   (   R1   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyt   __del__ë   s    c      
   C   s¦  |  j  j |  j r* |  j j d  d Sd } d |  j k rt |  j d rt d |  j d } |  j j d |  n  |  j j d |  |  j  j d |  |  j	 d	 g  \ } } yA |  j
 d
 d d d d  |  j d d  |  j j d  Wn& t k
 r} t j d |   n XyB t j t j |  j  j  r_t j   } |  j d |  n  Wn' t k
 r} |  j j d |  n Xt |  _ t |  _ Wd QXd S(   s#    initialize the shell connection.  s   initialization raceNs   exec /bin/sh -it   shells   exec %ss   custom  command shell: %ss!   running command shell:         %ss"    stty -echo ; unset HISTFILE ; %s
s   ^(.*[\$#%>\]])\s*$s    unset PROMPT_COMMAND ; s    unset HISTFILE ; s   PS1='PROMPT-$?->'; s   PS2=''; s   export PS1 PS2 2>&1 >/dev/null
t
   new_prompts   PROMPT-(\d+)->$s   got new shell prompts'   Shell startup on target host failed: %ss    cd %ss   local cd to %s failed(   R0   t   rlockR   R   t   warnR   R   R   t   writet   findt	   run_asynct
   set_promptt	   ExceptionR(   R)   t   sumisct   host_is_localt   surlt   UrlR   t   hostR   t   getcwdt   run_synct   warningR   R   t	   finalized(   R1   t   command_shellt   _t   outR2   t   pwd(    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR-   ó   s4    		c         C   se   yK | rJ |  j  rJ |  j  j ' |  j sA |  j  j   t |  _ n  Wd  QXn  Wn t k
 r` } n Xd  S(   N(   R0   R9   RH   R5   R   R?   (   R1   R4   R2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR5   3  s    	c      	   C   sP   |  j  j > y |  j  j |  SWn" t k
 rE } t j |   n XWd QXd S(   s   
        The shell is assumed to be alive if the shell processes lives.
        Attempt to restart shell if recover==True
        N(   R0   R9   t   aliveR?   t   ptyet   translate_exception(   R1   t   recoverR2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyRM   C  s
    c      	   C   s   |  j  j  yc d } d } x1 | d k rO |  j  j |  j g t  \ } } q W|  j |  \ } } | | f SWn" t k
 r } t j	 |   n XWd QXd S(   sf  
        If run_async was called, a command is running on the shell.  find_prompt
        can be used to collect its output up to the point where the shell prompt
        re-appears (i.e. when the command finishes).


        Note that this method blocks until the command finishes.  Future
        versions of this call may add a timeout parameter.
        N(
   R0   R9   R   R<   R   t   _PTY_TIMEOUTt   _eval_promptR?   RN   RO   (   R1   t   matcht   frett   rett   txtR2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyt   find_promptT  s    %iÿÿÿÿc      	   C   sV   |  j  j D y |  j  j | d | SWn" t k
 rK } t j |   n XWd QXd S(   sW   
        Note that this method blocks until pattern is found in the shell I/O.
        t   timeoutN(   R0   R9   R<   R?   RN   RO   (   R1   t   patternsRX   R2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR<   t  s
    c      	   C   s  d   } |  j  j è|  j } | |  _ t j d |  j t j  |  _ d } d } x.t ryó d |  j } | s{ d } n  |  j  j	 |  j g |  \ } } | d k r| d 7} | d k rÙ | |  _ t j d   n  |  j  j d  |  j j d	 |  | d 7} wV n  |  j |  \ }	 }
 |	 d k rM| |  _ t j d
 |   n  PWqV t k
 r} | |  _ t j | d   qV XqV W| d k rù|  j d  |  j  j	 d g d d \ } } | d k rì|  j d t  t j d   n  |  j   n  Wd QXd S(   s6  
        :type  new_prompt:  string 
        :param new_prompt:  a regular expression matching the shell prompt

        The new_prompt regex is expected to be a regular expression with one set
        of catching brackets, which MUST return the previous command's exit
        status.  This method will send a newline to the client, and expects to
        find the prompt with the exit value '0'.

        As a side effect, this method will discard all previous data on the pty,
        thus effectively flushing the pty output.  

        By encoding the exit value in the command prompt, we safe one roundtrip.
        The prompt on Posix compliant shells can be set, for example, via::

          PS1='PROMPT-$?->'; export PS1

        The newline in the example above allows to nicely anchor the regular
        expression, which would look like::

          PROMPT-(\d+)->$

        The regex is compiled with 're.DOTALL', so the dot character matches
        all characters, including line breaks.  Be careful not to match more
        than the exact prompt -- otherwise, a prompt search will swallow stdout
        data.  For example, the following regex::

          PROMPT-(.+)->$

        would capture arbitrary strings, and would thus match *all* of::

          PROMPT-0->ls
          data/ info
          PROMPT-0->

        and thus swallow the ls output...

        Note that the string match *before* the prompt regex is non-gready -- if
        the output contains multiple occurrences of the prompt, only the match
        up to the first occurence is returned.
        c         S   s   t  j d  } | j d |   S(   Ns
   \x1b[^m]*mt    (   R   R   t   sub(   RV   t   pat(    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyt   escape¯  s    s   ^(.*?)%s\s*$i    i
   g      ð?i   s2   Cannot use new prompt, parsing failed (10 retries)s   
s   sent prompt trigger again (%d)s   could not parse exit value (%s)s   Could not set shell prompts    printf "SYNCHRONIZE_PROMPT
"t   SYNCHRONIZE_PROMPTRX   R4   s&   Could not synchronize prompt detectionN(   R0   R9   R   R   R   R   R   R   R   R<   R   R(   t   BadParameterR;   R   R   RR   R?   RN   RO   R=   R5   R)   RW   (   R1   R8   R]   t
   old_promptt   retriest   triggerst   delayRT   RS   RU   RJ   R2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR>     sL    +					!
	
			!c   	   	   C   sv  |  j  j dy7|  j } |  j } | rJ | } t j d | t j  } n  d } | sr t j	 d | | f   n  | j
 |  } | s½ |  j j d | | f  t j	 d | | f   n  t | j    d k rÿ |  j j d |  t j	 d |   n  | j d  } t | j d   } | r9|  j |  n  | | f SWn% t k
 rk} t j | d   n XWd QXd S(	   sÔ   
        This method will match the given data against the current prompt regex,
        and expects to find an integer as match -- which is then returned, along
        with all leading data, in a tuple
        s   ^(.*)%s\s*$s/   cannot not parse prompt (%s), invalid data (%s)s    could not parse prompt (%s) (%s)i   s'   prompt does not capture exit value (%s)i   s   Could not eval promptN(   R0   R9   R   R   R   R   R   R   R(   R)   RS   R   R   t   lent   groupst   groupt   intR>   R?   RN   RO   (	   R1   t   dataR8   R   R   t   resultRV   RU   R2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyRR   û  s2    			c      	   C   s.  |  j  j |  j  j d t  sA t j d |  j  j     n  y¾| j   } | j d  ru t j	 d |   n  d } d } | t
 k r d } n  | t k r« d } n  | t k rÄ d	 | } n  | t k rÙ d
 } n  | t k rî d } n  | d k rd } n  |  j j d | | f  |  j  j d | | f  |  j } | rO| } n  |  j  j | g d d \ } } | d k r¢|  j d t  t j d |   n  |  j | |  \ }	 }
 d } d } | d k rÛt } n  | t
 k rên  | t k rÿ|
 } n  | t k r|
 } n  | t k s,| t k rÜ|
 } |  j  j d |  |  j  j |  j g d d \ } } | d k r|  j d t  t j d |   n  |  j |  \ } } | rÓt j d | | f   n  | } n  | t k rñd } n  |	 | | f SWn" t k
 r#} t j |   n XWd QXd S(   s9  
        Run a shell command, and report exit code, stdout and stderr (all three
        will be returned in a tuple).  The call will block until the command
        finishes (more exactly, until we find the prompt again on the shell's
        I/O stream), and cannot be interrupted.

        :type  command: string
        :param command: shell command to run.  
        
        :type  iomode:  enum
        :param iomode:  Defines how stdout and stderr are captured.  

        :type  new_prompt:  string 
        :param new_prompt:  regular expression matching the prompt after
        command succeeded.

        We expect the ``command`` to not to do stdio redirection, as this is we want
        to capture that separately.  We *do* allow pipes and stdin/stdout
        redirection.  Note that SEPARATE mode will break if the job is run in
        the background

        
        The following iomode values are valid:

          * *IGNORE:*   both stdout and stderr are discarded, `None` will be
                        returned for each.
          * *MERGED:*   both streams will be merged and returned as stdout; 
                        stderr will be `None`.  This is the default.
          * *SEPARATE:* stdout and stderr will be captured separately, and
                        returned individually.  Note that this will require 
                        at least one more network hop!  
          * *STDOUT:*   only stdout is captured, stderr will be `None`.
          * *STDERR:*   only stderr is captured, stdout will be `None`.
          * *None:*     do not perform any redirection -- this is effectively
                        the same as `MERGED`

        If any of the requested output streams does not return any data, an
        empty string is returned.

        
        If the command to be run changes the prompt to be expected for the
        shell, the ``new_prompt`` parameter MUST contain a regex to match the
        new prompt.  The same conventions as for set_prompt() hold -- i.e. we
        expect the prompt regex to capture the exit status of the process.
        RP   s#   Can't run command -- shell died:
%st   &s,   run_sync can only run foreground jobs ('%s')RZ   s"   /tmp/saga-python.ssh-job.stderr.$$s    1>>/dev/null 2>>/dev/nulls    2>&1s    2>%ss    2>/dev/nulls    2>&1 1>/dev/nulls   run_sync: %s%ss   %s%s
RX   g      ð¿R4   s   run_sync failed, no prompt (%s)s    cat %s
s#   run_sync failed, no stderr (%s: %s)N(   R0   R9   RM   R   R(   t   IncorrectStatet   autopsyt   stript   endswithR_   t   IGNOREt   MERGEDt   SEPARATEt   STDOUTt   STDERRR   R   R   R;   R   R<   R5   RR   R?   RN   RO   (   R1   t   commandt   iomodeR8   t   redirt   _errR   RT   RS   RU   RV   t   stdoutt   stderrt   _rett   _stderrR2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyRF   /  s|    /									!			$				c      	   C   s   |  j  j  |  j  j d t  sA t j d |  j  j     n  y! | j   } |  j d |  Wn" t	 k
 r } t
 j |   n XWd QXd S(   s¿  
        Run a shell command, but don't wait for prompt -- just return.  It is up
        to caller to eventually search for the prompt again (see
        :func:`find_prompt`.  Meanwhile, the caller can interact with the called
        command, via the I/O channels.

        :type  command: string
        :param command: shell command to run.  

        For async execution, we don't care if the command is doing i/o redirection or not.
        RP   s   Cannot run command:
%ss    %s
N(   R0   R9   RM   R   R(   Rk   Rl   Rm   t   sendR?   RN   RO   (   R1   Rt   R2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR=   Ç  s    	c      	   C   s   |  j  j v |  j  j d t  sA t j d |  j  j     n  y |  j  j d |  Wn" t k
 r} } t	 j
 |   n XWd QXd S(   sB   
        send data to the shell.  No newline is appended!
        RP   s   Cannot send data:
%ss   %sN(   R0   R9   RM   R   R(   Rk   Rl   R;   R?   RN   RO   (   R1   Rh   R2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR|   ç  s    	c         C   s   yn |  j  d t |   } t | d  } | j |  | j   | j   |  j | |  } t j |  | SWn" t	 k
 r } t
 j |   n Xd S(   sF  
        :type  src: string
        :param src: data to be staged into the target file

        :type  tgt: string
        :param tgt: path to target file to staged to
                    The tgt path is not an URL, but expected to be a path
                    relative to the shell's URL.

        The content of the given string is pasted into a file (specified by tgt)
        on the remote system.  If that file exists, it is overwritten.
        A NoSuccess exception is raised if writing the file was not possible
        (missing permissions, incorrect path, etc.).
        s   /staging.%st   wbN(   R!   t   idt   openR;   t   flusht   closet   stage_to_remoteR   t   removeR?   RN   RO   (   R1   t   srct   tgtt   fnamet   fhandleRU   R2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyt   write_to_remoteú  s    

c         C   s   yc |  j  d t |   } |  j | |  } t | d  } | j   } | j   t j |  | SWn" t k
 r } t	 j
 |   n Xd S(   sÖ   
        :type  src: string
        :param src: path to source file to staged from
                    The src path is not an URL, but expected to be a path
                    relative to the shell's URL.
        s   /staging.%st   rN(   R!   R~   t   stage_from_remoteR   t   readR   R   R   R?   RN   RO   (   R1   R   R   RJ   R   RK   R2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyt   read_from_remote"  s    
RZ   c         C   s@   y |  j  | | |  SWn" t k
 r; } t j |   n Xd S(   s¹  
        :type  src: string
        :param src: path of local source file to be stage from.
                    The tgt path is not an URL, but expected to be a path
                    relative to the current working directory.

        :type  tgt: string
        :param tgt: path to target file to stage to.
                    The tgt path is not an URL, but expected to be a path
                    relative to the shell's URL.
        N(   t   run_copy_toR?   RN   RO   (   R1   R   R   t   cp_flagsR2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR   A  s    c         C   s@   y |  j  | | |  SWn" t k
 r; } t j |   n Xd S(   s¶  
        :type  src: string
        :param tgt: path to source file to stage from.
                    The tgt path is not an URL, but expected to be a path
                    relative to the shell's URL.

        :type  tgt: string
        :param src: path of local target file to stage to.
                    The tgt path is not an URL, but expected to be a path
                    relative to the current working directory.
        N(   t   run_copy_fromR?   RN   RO   (   R1   R   R   R   R2   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR   Y  s    c      
   C   s  |  j  j ó|  j } t i | d 6| d 6| d 6j   | j    } | d | d d | } | d | d d | } |  j s |  j j | |  |  _ n  d } d	 | k rd
 d l }	 |	 j |  }
 xF |
 D]; } t	 j
 j |  rÓ | d | t	 j
 j |  f 7} qÓ qÓ Wn  |  j j d | | f  } |  j j d g d
  \ } } |  j j d g d  \ } } d | k rt j j | d d t |    n  d | k rÑt j j | d d t |    n  d | k rt j j | d d t |    n  d	 | k r>d | k r>t j j | d d |   q>n  | j d  } g  } x | D] } | j d d  } | rZ| d } | rÅ| d d! k r¥| d } n  | d
 d" k rÅ| d
  } qÅn  | rÞ| j |  qÞqZqZW| d j d  |  | SWd QXd S(#   s   
        This initiates a slave copy connection.   Src is interpreted as local
        path, tgt as path on the remote host.

        Now, this is ugly when over sftp: sftp supports recursive copy, and
        wildcards, all right -- but for recursive copies, it wants the target
        dir to exist -- so, we have to check if the local src is a  dir, and if
        so, we first create the target before the copy.  Worse, for wildcards we
        have to do a local expansion, and the to do the same for each entry...
        R   R   R   t   scriptst   typet   copy_tot
   copy_to_inRZ   t   sftpiÿÿÿÿNs   mkdir %s/%s
s   %s%s
s   [\$\>\]]\s*$g      ð?s   Invalid flagR   s   sftp version not supported (%s)s   No such file or directorys   file copy failed: %ss   is not a directorys   File copy failed: %ss	   not founds   
t    i   i    t   't   "t   `i   s   copy done: %s(   R   R   R   (   R   R   R   (   R0   R9   R.   t   dictt   itemsR   R,   t   get_cp_slavet   globR   R&   R'   t   basenameR;   R<   R(   R)   t   _logt   strt   DoesNotExistR_   t   splitt   appendR   (   R1   R   R   R   R   t   replt   s_cmdt   s_int   prepR   t   src_listt   sRJ   RK   t   linest   filest   linet   elemst   f(    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR   q  sV    		*&&&#
  c         C   s!  |  j  j |  j } t i | d 6| d 6| d 6j   | j    } | d | d d | } | d | d d | } |  j s |  j j | |  |  _ n  d } d	 | k r?|  j j d
 |  |  j j	 d g d  \ }	 }
 |
 d j
 d  } xF | D]; } t j j |  rý | d | t j j |  f 7} qý qý Wn  |  j j d | | f  }	 |  j j	 d g d  \ }	 }
 d |
 k r¥t j j | d d |
   n  d |
 k rÑt j j | d d |
   n  d |
 k rýt j j | d d |
   n  d	 | k r8d |
 k r8t j j | d d |
   q8n  |
 j
 d  } g  } x® | D]¦ } | j
 d d  } | rTt |  d k rT| d d k rT| d } | rá| d d# k rÁ| d } n  | d d$ k rá| d  } qán  | rú| j |  qúqTqTW| d j d! |  | SWd" QXd" S(%   s   
        This initiates a slave copy connection.   Src is interpreted as path on
        the remote host, tgt as local path.

        We have to do the same mkdir trick as for the run_copy_to, but here we
        need to expand wildcards on the *remote* side :/
        R   R   R   R   R   t	   copy_fromt   copy_from_inRZ   R   s    ls %s
s   ^sftp> iÿÿÿÿi   s   /ns   lmkdir %s/%s
s   %s%s
s   [\$\>\]] *$s   Invalid flagR   s   sftp version not supported (%s)s   No such file or directorys   file copy failed: %ss   is not a directorys	   not founds   
R   i   i    t   FetchingR   R   R   s   copy done: %sN(   R   R   R   (   R   R   R   (   R0   R9   R.   R   R   R   R,   R   R;   R<   R¡   R   R&   R'   R   R(   R)   R   R    R_   Rd   R¢   R   (   R1   R   R   R   R   R£   R¤   R¥   R¦   RJ   RK   R§   R¨   R©   Rª   R«   R¬   R­   (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR   Ê  sV    			*   #(
  N(   t   __name__t
   __module__t   __doc__R   R3   R6   R-   R   R5   RM   RW   R<   R>   RR   RF   R=   R|   R   R   R   R   R   R   (    (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyR   (   s&   
7		@	 	w4	 		(	Y($   t
   __author__t   __copyright__t   __license__R   R   t   sysR$   t   saga.utils.misct   utilst   miscR@   t   radical.utils.loggerR   R   t   saga.utils.pty_shell_factoryt   pty_shell_factoryR*   t   saga.urlR   RB   t   saga.exceptionst
   exceptionsR(   t   saga.sessionR	   R
   t   pty_exceptionsRN   RQ   Ro   Rp   Rq   Rr   Rs   t   objectR   (    (    (    sh   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/saga_python-0.18-py2.7.egg/saga/utils/pty_shell.pyt   <module>   s(   